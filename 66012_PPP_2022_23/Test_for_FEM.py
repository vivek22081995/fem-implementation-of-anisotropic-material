# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# Test_for_FEM.py - Python test file for testing the FEM implementation
#-----------------------------------------------------------------------------------------------------------------------#


import numpy as np
from Functions import *
import pytest
from Element_routine_linear import *


def test_shape_function_sum():
    '''
    SANITY CHECK
    Aim : Test the sanity of the shape functions used

    Expected result : Sum of the all the 8 shape functions for each gauss point should evaluate to 1

    Test Command : pytest Test_for_FEM.py::test_shape_function_sum

    Remarks : Test case passed successfully     
    '''
    
    g=1/np.sqrt(3)
    
    GP=np.array([[-g ,-g ,-g],[g ,-g, -g],[g ,g ,-g],[-g, g ,-g],[-g, -g, g],[g, -g, g],[g, g, g],[-g, g, g]])
    result=np.zeros(8)
    n=0
    for i in range(8):
        s=GP[i,0]
        t=GP[i,1]
        u=GP[i,2]
        
        N1,N2,N3,N4,N5,N6,N7,N8=shape_functions(s,t,u)
        result[n]=np.round(N1+N2+N3+N4+N5+N6+N7+N8)
        n+=1
        flag=0
    if np.all(result==1)==True:
        flag=1
    assert(flag==1) is True
        
def test_quad_shape_functions_sum():
    '''
    SANITY CHECK
    Aim : Test the sanity of the shape functions used

    Expected result : Sum of the all the 20 shape functions for each gauss point should evaluate to 1

    Test Command : pytest Test_for_FEM.py::test_quad_shape_functions_sum

    Remarks : Test case passed successfully     
    '''
    g=0.7746
    GP=np.array([[-g,-g,-g],[0,-g,-g],[g,-g,-g],[-g,0,-g],[0,0,-g],[g,0,-g],[-g,g,-g],[0,g,-g],[g,g,-g],[-g,-g,0],[0,-g,0],\
                 [g,-g,0],[-g,0,0],[0,0,0],[g,0,0],[-g,g,0],[0,g,0],[g,g,0],[-g,-g,g],[0,-g,g],[g,-g,g],[-g,0,g],[0,0,g],[g,0,g],[-g,g,g],[0,g,g],[g,g,g]])
    n=0
    
    result=np.zeros(27)
    
    for i in range(27):
        s=GP[i,0]
        t=GP[i,1]
        u=GP[i,2]
        
        N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20=shape_functions_quadratic(s, t, u)
        result[n]=np.round(N1+N2+N3+N4+N5+N6+N7+N8+N9+N10+N11+N12+N13+N14+N15+N16+N17+N18+N19+N20,8)
        n+=1
        flag=0
    if np.all(result==1)==True:
        flag=1
    assert(flag==1) is True
        

def test_derivative_shape_function_sum():
    '''
    SANITY CHECK
    Aim : Test the sanity of the derivative of the shape functions used

    Expected result : Sum of the derivative of the shape function along the respective columns should be 0 for each gauss point

    Test Command : pytest Test_for_FEM.py::test_derivative_shape_function_sum

    Remarks : Test case passed successfully      
    '''

    g=1/np.sqrt(3)
    
    GP=np.array([[-g ,-g ,-g],[g ,-g, -g],[g ,g ,-g],[-g, g ,-g],[-g, -g, g],[g, -g, g],[g, g, g],[-g, g, g]])
    result=np.zeros([3,8])
    n=0
    for i in range(8):
        s=GP[i,0]
        t=GP[i,1]
        u=GP[i,2]
        N_deriv_mat=deri_N_mat(s,t,u)
        result[0,n]=np.round(sum(N_deriv_mat[0,:]),8)
        result[1,n]=np.round(sum(N_deriv_mat[1,:]),8)
        result[2,n]=np.round(sum(N_deriv_mat[2,:]),8)
        n+=1
    flag = 0
    if np.all(result==0) == True:
        flag = 1
    assert (flag == 1) is True


def test_quad_der_shape_function():
   '''
   SANITY CHECK
   Aim : Test the sanity of the derivative of the shape functions used

   Expected result : Sum of the derivative of the shape function along the respective columns should be 0 for each gauss point

   Test Command : pytest Test_for_FEM.py::test_quad_der_shape_function_sum

   Remarks : Test case passed successfully      
   '''
   
   g=0.7746
   GP=np.array([[-g,-g,-g],[0,-g,-g],[g,-g,-g],[-g,0,-g],[0,0,-g],[g,0,-g],[-g,g,-g],[0,g,-g],[g,g,-g],[-g,-g,0],[0,-g,0],\
                [g,-g,0],[-g,0,0],[0,0,0],[g,0,0],[-g,g,0],[0,g,0],[g,g,0],[-g,-g,g],[0,-g,g],[g,-g,g],[-g,0,g],[0,0,g],[g,0,g],[-g,g,g],[0,g,g],[g,g,g]])
   n=0
   
   result=np.zeros([3,27])
   
   for i in range(27):
       s=GP[i,0]
       t=GP[i,1]
       u=GP[i,2]
       N_deriv_mat=der_quad_shape_functions(s, t, u)
       result[0,n]=np.round(sum(N_deriv_mat[0,:]),4)
       result[1,n]=np.round(sum(N_deriv_mat[1,:]),4)
       result[2,n]=np.round(sum(N_deriv_mat[2,:]),4)
       n+=1
   flag = 0
   if np.all(result==0) == True:
       flag = 1
   assert (flag == 1) is True   


def test_rotated_determinant_jacobian():
    '''
    SANITY CHECK
    Aim : Test the volume conserving nature of Jacobian even after rotation with an angle

    Expected result : The determinant of the Jacobian should not change even after rotation

    Test Command : pytest Test_for_XFEM.py::test_rotated_determinant_jacobian

    Remarks : Test case passed successfully     
    
    '''
    g=1/np.sqrt(3)
        
    GP=np.array([[-g ,-g ,-g],[g ,-g, -g],[g ,g ,-g],[-g, g ,-g],[-g, -g, g],[g, -g, g],[g, g, g],[-g, g, g]])
    
    coord=np.array([[0,0,0],[1,0,0],[1,1,0],[0,1,0],[0,0,1],[1,0,1],[1,1,1],[0,1,1]])
    
    result=np.zeros([360,8])
    
    
    n = 0
    for i in range(8):
        s=GP[i,0]
        t=GP[i,1]
        u=GP[i,2]
        
        N_der_mat = deri_N_mat(s,t,u)
        Jacobian = N_der_mat @ coord
        
        for k in range(0,360):
            theta = np.radians(k)
            rotationx=np.array([[1,0,0],[0,np.cos(theta),-np.sin(theta)],[0,np.sin(theta),np.cos(theta)]])
            rotated_J=rotationx@Jacobian
            result[k,n]=np.round(np.linalg.det(rotated_J),8)
            
            k+=1
        n+=1
    flag=0
    actual_det=np.round(np.linalg.det(deri_N_mat(g,g,g)@coord),8)
    if np.all(result==actual_det):
        flag=1
    assert(flag==1) is True


def test_rotated_quad_determinant_jacobian():
    '''
    SANITY CHECK
    Aim : Test the volume conserving nature of Jacobian even after rotation with an angle

    Expected result : The determinant of the Jacobian should not change even after rotation

    Test Command : pytest Test_for_FEM.py::test_rotated_quad_determinant_jacobian

    Remarks : Test case passed successfully     
    
    '''
    
    coord=np.array([[-1,-1,-1],[-1,-1,0],[-1,-1,1],[0,-1,1],[1,-1,1],[1,-1,0],[1,-1,-1],[0,-1,-1],[-1,0,-1],[-1,0,1],[1,0,1],[1,0,-1],\
                    [-1,1,-1],[-1,1,0],[-1,1,1],[0,1,1],[1,1,1],[1,1,0],[1,1,-1],[0,1,-1]])
    
    g=0.774596669241483
    GP=np.array([[-g,-g,-g],[0,-g,-g],[g,-g,-g],[-g,0,-g],[0,0,-g],[g,0,-g],[-g,g,-g],[0,g,-g],[g,g,-g],[-g,-g,0],[0,-g,0],\
                 [g,-g,0],[-g,0,0],[0,0,0],[g,0,0],[-g,g,0],[0,g,0],[g,g,0],[-g,-g,g],[0,-g,g],[g,-g,g],[-g,0,g],[0,0,g],[g,0,g],[-g,g,g],[0,g,g],[g,g,g]])
        
    result=np.zeros([360,27])  
       
    for i in range(27):
        s=GP[i,0]
        t=GP[i,1]
        u=GP[i,2]
    
        N_der_mat = der_quad_shape_functions(s, t, u)
        Jacobian = N_der_mat @ coord
        n=0
        for k in range(0,360):
            theta = np.radians(k)
            rotationx=np.array([[1,0,0],[0,np.cos(theta),-np.sin(theta)],[0,np.sin(theta),np.cos(theta)]])
            rotated_J=rotationx@Jacobian
            result=np.round(np.linalg.det(rotated_J),8)    
        n+=1
    flag=0           
    actual_det=np.round(np.linalg.det(der_quad_shape_functions(g,g,g)@coord),8)
    
    if np.all(result==actual_det):
        flag=1
    assert(flag==1) is True
    
    
def test_stiffness_matrix_singularity():
    '''
    SANITY CHECK
    Aim : Testing the positive definite nature of the global stiffness matrix

    Expected result : All the eigen values of the global stiffness matrix should be non negative

    Test Command : pytest Test_for_FEM.py::test_stiffness_matrix_singularity

    Remarks : Test case passed successfully     
    '''
    
    E=210e3;
    NU=0.3;
    D= E/((1+NU)*(1-2*NU))*np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                    [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                    [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])
    
    nelx=2      
    nely=1      
    nelz=1      
    L=10         
    W=10         
    H=10         
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)    #element and node list is defined here
    NOE=nelx*nely*nelz                                   #Number of Elements
    NON=(nelx+1)*(nely+1)*(nelz+1)                       #Number of Nodes
    u_k_list_new =[np.zeros([3*NON,1]).tolist()]         #Global displacement list
    K_global=np.zeros([3*NON,3*NON])                          #Global stiffness Matrix
    for en in range(NOE):
        u_k=np.array(u_k_list_new[-1])
        u_k[24]=u_k[27]=u_k[30]=u_k[33]=2
        con=np.array(connectivity[en],dtype=int)
        con_list=con.tolist()
        u_e=np.zeros([24,1])
        i=0
        for e in con_list:
            u_e[3*i:3*(i+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
            i+=1
        stress_list,strain_list,K_ele,F_int_ele=element_routine(D,en,connectivity,coord,u_e)
        #assembly of global Stiffness Matrix
        for i in range(len(con_list)):
            for j in range(len(con_list)):
                K_global[3*(int(con_list[i])-1):3*int((con_list[i])),3*(int(con_list[j])-1):3*(int(con_list[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
        eig_val,eig_vec = np.linalg.eig(K_global)
    assert (all(np.round(eig_val,8)>= 0)) is True
    
def test_rigid_body_constraint():
    '''
    PATCH TESTING
    Aim : Eigen value test for computing the number of eigen values which are zero. This corresponds to the rigid body motion of the structure

    Expected result : Due to the unconstrained nature of the current 3D problem, 6 zero eigen values are expected.
                      (3 translation and 3 rotation)

    Test Command : pytest Test_for_FEM.py::test_rigid_body_constraint

    Remarks : Test case passed successfully     
    '''
    E=210e3;
    NU=0.3;
    D= E/((1+NU)*(1-2*NU))*np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                        [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                        [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])     
    nelx=1      
    nely=1      
    nelz=1      
    L=10        
    W=10        
    H=10        
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)    
    NOE=nelx*nely*nelz
    NON=(nelx+1)*(nely+1)*(nelz+1)                       
    u_k_list_new =[np.zeros([3*NON,1]).tolist()]         
    K_global=np.zeros([3*NON,3*NON])                          
    for en in range(NOE):
        u_k=np.array(u_k_list_new[-1])
        u_k[12]=u_k[15]=u_k[18]=u_k[21]=2
        con=np.array(connectivity[en],dtype=int)
        con_list=con.tolist()
        u_e=np.zeros([24,1])
        i=0
        for e in con_list:
            u_e[3*i:3*(i+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
            i+=1
        stress_list,strain_list,K_ele,F_int_ele=element_routine(D,en,connectivity,coord,u_e)
        #assembly of global Stiffness Matrix
        for i in range(len(con_list)):
            for j in range(len(con_list)):
                K_global[3*(int(con_list[i])-1):3*int((con_list[i])),3*(int(con_list[j])-1):3*(int(con_list[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
        eig_val,eig_vec = np.linalg.eig(K_global)
        unconstrained_dof = np.round(eig_val,5).shape[0] - np.count_nonzero(np.round(eig_val,5))
        assert (unconstrained_dof == 6) is True

##############################################################################################################################
''' All the functions listed below are same as the functions used in the main program  
'''
###############################################################################################################################


def deri_N_mat(s,t,u):
    
    dN1s = -((t - 1)*(u - 1))/8   ### differentiating wrt s ######
    dN2s = ((t - 1)*(u - 1))/8
    dN3s = -((t + 1)*(u - 1))/8
    dN4s =((t + 1)*(u - 1))/8
    dN5s = ((t - 1)*(u + 1))/8
    dN6s = -((t - 1)*(u + 1))/8
    dN7s = ((t + 1)*(u + 1))/8
    dN8s = -((t + 1)*(u + 1))/8
    
    dN1t = -(s/8 - 1/8)*(u - 1)      ##### differentiating wrt t ######
    dN2t = (s/8 + 1/8)*(u - 1)
    dN3t = -(s/8 + 1/8)*(u - 1)
    dN4t = (s/8 - 1/8)*(u - 1)
    dN5t = (s/8 - 1/8)*(u + 1)
    dN6t = -(s/8 + 1/8)*(u + 1)
    dN7t = (s/8 + 1/8)*(u + 1)
    dN8t = -(s/8 - 1/8)*(u + 1)
    
    dN1u = -(s/8 - 1/8)*(t - 1)      ###### differentiating wrt u ##########
    dN2u = (s/8 + 1/8)*(t - 1)
    dN3u = -(s/8 + 1/8)*(t + 1)
    dN4u = (s/8 - 1/8)*(t + 1)
    dN5u = (s/8 - 1/8)*(t - 1)
    dN6u = -(s/8 + 1/8)*(t - 1)
    dN7u = (s/8 + 1/8)*(t + 1)
    dN8u = -(s/8 - 1/8)*(t + 1)
    
    dN_mat=np.array([[dN1s,dN2s,dN3s,dN4s,dN5s,dN6s,dN7s,dN8s],\
                    [dN1t,dN2t,dN3t,dN4t,dN5t,dN6t,dN7t,dN8t],\
                    [dN1u,dN2u,dN3u,dN4u,dN5u,dN6u,dN7u,dN8u]])
    return dN_mat  


def shape_functions(s,t,u):
    N1 = 1/8*(1-s)*(1-t)*(1-u) 
    N2 = 1/8*(1+s)*(1-t)*(1-u)
    N3 = 1/8*(1+s)*(1+t)*(1-u)
    N4 = 1/8*(1-s)*(1+t)*(1-u)
    N5 = 1/8*(1-s)*(1-t)*(1+u)
    N6 = 1/8*(1+s)*(1-t)*(1+u)
    N7 = 1/8*(1+s)*(1+t)*(1+u) 
    N8 = 1/8*(1-s)*(1+t)*(1+u)
    
    
    return N1,N2,N3,N4,N5,N6,N7,N8

def shape_functions_quadratic(s,t,u):
    
    N1=(1/8)*(1-s)*(1-t)*(1-u)*(-s-t-u-2)
    N2=(1/4)*(1-s)*(1-t)*(1-u**2)
    N3=(1/8)*(1-s)*(1-t)*(1+u)*(-s-t+u-2)
    N4=(1/4)*(1-s**2)*(1-t)*(1+u)
    N5=(1/8)*(1+s)*(1-t)*(1+u)*(s-t+u-2)
    N6=(1/4)*(1+s)*(1-t)*(1-u**2)
    N7=(1/8)*(1+s)*(1-t)*(1-u)*(s-t-u-2)
    N8=(1/4)*(1-s**2)*(1-t)*(1-u)
    N9=(1/4)*(1-s)*(1-t**2)*(1-u)
    N10=(1/4)*(1-s)*(1-t**2)*(1+u)
    N11=(1/4)*(1+s)*(1-t**2)*(1+u)
    N12=(1/4)*(1+s)*(1-t**2)*(1-u)
    N13=(1/8)*(1-s)*(1+t)*(1-u)*(-s+t-u-2)
    N14=(1/4)*(1-s)*(1+t)*(1-u**2)
    N15=(1/8)*(1-s)*(1+t)*(1+u)*(-s+t+u-2)
    N16=(1/4)*(1-s**2)*(1+t)*(1+u)
    N17=(1/8)*(1+s)*(1+t)*(1+u)*(s+t+u-2)
    N18=(1/4)*(1+s)*(1+t)*(1-u**2)
    N19=(1/8)*(1+s)*(1+t)*(1-u)*(s+t-u-2)
    N20=(1/4)*(1-s**2)*(1+t)*(1-u)
        
    return N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20


def der_quad_shape_functions(s,t,u):
    
    dN1s=(1/8)*(1-t)*(1-u)*(2*s+t+u+1)       ##############diff wrt s##############
    dN2s=(-1/4)*(1-t)*(1-u**2)
    dN3s=(1/8)*(1-t)*(1+u)*(2*s+t-u+1)
    dN4s=(-1/2)*s*(1-t)*(1+u)
    dN5s=(1/8)*(1-t)*(1+u)*(2*s-t+u-1)
    dN6s=(1/4)*(1-t)*(1-u**2)
    dN7s=(1/8)*(1-t)*(1-u)*(2*s-t-u-1)
    dN8s=(-1/2)*s*(1-t)*(1-u)
    dN9s=(-1/4)*(1-t**2)*(1-u)
    dN10s=(-1/4)*(1-t**2)*(1+u)
    dN11s=(1/4)*(1-t**2)*(1+u)
    dN12s=(1/4)*(1-t**2)*(1-u)
    dN13s=(1/8)*(1+t)*(1-u)*(2*s-t+u+1)
    dN14s=(-1/4)*(1+t)*(1-u**2)
    dN15s=(1/8)*(1+t)*(1+u)*(2*s-t-u+1)
    dN16s=(-1/2)*(s)*(1+t)*(1+u)
    dN17s=(1/8)*(1+t)*(1+u)*(2*s+t+u-1)
    dN18s=(1/4)*(1+t)*(1-u**2)
    dN19s=(1/8)*(1+t)*(1-u)*(2*s+t-u-1)
    dN20s=(-1/2)*(s)*(1+t)*(1-u)
    
    dN1t=(1/8)*(1-s)*(1-u)*(s+2*t+u+1)       ##############diff wrt s##############
    dN2t=(-1/4)*(1-s)*(1-u**2)
    dN3t=(1/8)*(1-s)*(1+u)*(s+2*t-u+1)
    dN4t=(-1/4)*(1-s**2)*(1+u)
    dN5t=(1/8)*(1+s)*(1+u)*(-s+2*t-u+1) 
    dN6t=(-1/4)*(1+s)*(1-u**2)
    dN7t=(1/8)*(1+s)*(1-u)*(-s+2*t+u+1)
    dN8t=(-1/4)*(1-s**2)*(1-u)
    dN9t=(-1/2)*(1-s)*(t)*(1-u)
    dN10t=(-1/2)*(1-s)*(t)*(1+u)
    dN11t=(-1/2)*(1+s)*(t)*(1+u)
    dN12t=(-1/2)*(1+s)*(t)*(1-u)
    dN13t=(1/8)*(1-s)*(1-u)*(-s+2*t-u-1)
    dN14t=(1/4)*(1-s)*(1-u**2)
    dN15t=(1/8)*(1-s)*(1+u)*(-s+2*t+u-1)
    dN16t=(1/4)*(1-s**2)*(1+u)
    dN17t=(1/8)*(1+s)*(1+u)*(s+2*t+u-1)
    dN18t=(1/4)*(1+s)*(1-u**2)
    dN19t=(1/8)*(1+s)*(1-u)*(s+2*t-u-1)
    dN20t=(1/4)*(1-s**2)*(1-u)
    
    dN1u=(1/8)*(1-s)*(1-t)*(s+t+2*u+1)       ##############diff wrt s##############
    dN2u=(-1/2)*(1-s)*(1-t)*(u)
    dN3u=(1/8)*(1-s)*(1-t)*(-s-t+2*u-1)
    dN4u=(1/4)*(1-s**2)*(1-t)
    dN5u=(1/8)*(1+s)*(1-t)*(s-t+2*u-1)
    dN6u=(-1/2)*(1+s)*(1-t)*(u)
    dN7u=(1/8)*(1+s)*(1-t)*(-s+t+2*u+1)
    dN8u=(-1/4)*(1-s**2)*(1-t)
    dN9u=(-1/4)*(1-s)*(1-t**2)
    dN10u=(1/4)*(1-s)*(1-t**2)
    dN11u=(1/4)*(1+s)*(1-t**2)
    dN12u=(-1/4)*(1+s)*(1-t**2)
    dN13u=(1/8)*(1-s)*(1+t)*(s-t+2*u+1)
    dN14u=(-1/2)*(1-s)*(1+t)*(u)
    dN15u=(1/8)*(1-s)*(1+t)*(-s+t+2*u-1)
    dN16u=(1/4)*(1-s**2)*(1+t)
    dN17u=(1/8)*(1+s)*(1+t)*(s+t+2*u-1)
    dN18u=(-1/2)*(1+s)*(1+t)*(u)
    dN19u=(1/8)*(1+s)*(1+t)*(-s-t+2*u+1)
    dN20u=(-1/4)*(1-s**2)*(1+t)
    
    dN_mat=np.array([[dN1s,dN2s,dN3s,dN4s,dN5s,dN6s,dN7s,dN8s,dN9s,dN10s,dN11s,dN12s,dN13s,dN14s,dN15s,dN16s,dN17s,dN18s,dN19s,dN20s],\
                    [dN1t,dN2t,dN3t,dN4t,dN5t,dN6t,dN7t,dN8t,dN9t,dN10t,dN11t,dN12t,dN13t,dN14t,dN15t,dN16t,dN17t,dN18t,dN19t,dN20t],\
                    [dN1u,dN2u,dN3u,dN4u,dN5u,dN6u,dN7u,dN8u,dN9u,dN10u,dN11u,dN12u,dN13u,dN14u,dN15u,dN16u,dN17u,dN18u,dN19u,dN20u]])
    
    return dN_mat

