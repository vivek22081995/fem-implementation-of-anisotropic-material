# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# driver_routine_Hill.py - Python file to verify material_routine Hill by performing simple tensile test
#-------------------------------------------------------------------------------------------------------------------------#

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from Material_routine_isotropic import *
from Functions import *
from tqdm import tqdm

def test_isotropic():
    '''
    Tensile TESTING
    Aim : Verification of the isotropic hill criterion material routine for a simple tensile test

    Expected result : Non linear stress v/s strain graph with inital linear elasticity and showing non linear region(plastic region) 

    Test command : test_case_isotropic.py

    Remarks : Test case passed successfully
    
    '''
    strain_list_final= [np.zeros((6,1))]
    stress_list_final=[np.zeros((6,1))]
    
    
    
    #=======================Isotropic Elasticity matrix======================================================
    E=210e3
    NU=0.3
    C = (E/((1+NU)*(1-2*NU)))*np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                        [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                        [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])
    
    de=0.00005          #incremental strain
    load=1              #range of strain 
    if load==1:
      emax=0.01
    elif load==2:
      emax=0.015;
    elif load==3:
      emax=0.02;
    
    cycles=3            #number of cycles
    
    emin=-emax
    cyclic_tensile_load = np.linspace(0,emax,int(emax/de + 1),endpoint=True)
    if cycles==0:
        strain11=cyclic_tensile_load
    elif cycles>=1:
        cyclic_compressive_load = np.linspace(emax,emin,int((emax-emin)/de),endpoint=False)
        cyclic_compressive_load = np.delete(cyclic_compressive_load,0)
        step = int((emax-emin)/de + 1)
        cyclic_tensile_reload = np.linspace(emin,emax,step, endpoint=True)
        hys = np.append(cyclic_compressive_load,cyclic_tensile_reload,axis=0)
        strain11 = np.append(cyclic_tensile_load,hys,axis=0);
        if cycles>1:
            for i in range(1,cycles):
                strain11=np.append(strain11,hys)            
        steps=strain11.size - 1
    
     
    step=1
    
    ############Initiaizing variables####################
    plastic_strain=np.zeros([6,1])
    eff_plastic_strain=0
    back_stress1=np.zeros([6,1])
    back_stress2=np.zeros([6,1])
    back_stress3=np.zeros([6,1])
    drag_stress=0
    
    
    stress11   = np.empty(steps+1);
    stress22   = np.empty(steps+1);
    stress33   = np.empty(steps+1);
    stress12   = np.empty(steps+1);
    stress13   = np.empty(steps+1);
    stress23   = np.empty(steps+1);
    
    strain22   = np.empty(steps+1);
    strain33   = np.empty(steps+1);
    plastic11    = np.empty(steps+1);
    
    
    #calling material routine for all the steps of strain increment############
    for n in tqdm(range(steps)):
        #print(n)
        epsilon1=np.array([1,-0.5,-0.5,0,0,0])
        
        epsilon=np.multiply(epsilon1,strain11[n+1]) 
        
        strain22[n+1]=epsilon[1]
        
        strain33[n+1]=epsilon[2]
        strain_list_final.append(epsilon)
        C_ep,stress_new,back_stress1,back_stress2,back_stress3,drag_stress,plastic_strain,eff_plastic_strain=material_routine(C,epsilon,plastic_strain,back_stress1,back_stress2,back_stress3,drag_stress,eff_plastic_strain,step)
        stress_list_final.append(stress_new)
        
        stress11[n+1]=stress_new[0]
        stress22[n+1]=stress_new[1]
        stress33[n+1]=stress_new[2]
        stress12[n+1]=stress_new[3]
        stress13[n+1]=stress_new[4]
        stress23[n+1]=stress_new[5]
        plastic11[n+1]=plastic_strain[0]
        
        step +=1
        
    #plotting  
    fig1, ax = plt.subplots()
    fig1.set_size_inches(16,9)
    ax.plot(strain11[4:],stress11[4:], label = r'$\sigma_{11}$')
    ax.set_xlabel(r'$\epsilon_{11}$', fontsize = 16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    ax.set_ylabel(r'$\sigma$ in MPa',fontsize = 16)
    ax.legend(loc=5,prop={'size': 12})
    plt.grid(color = 'black', linestyle = '--', linewidth = 0.5)
    plt.show()
    
       
    fig3 = plt.figure()
    fig3.set_size_inches(16,9)
    x=np.linspace(0,steps,steps+1);
    plt.xlabel(r'step number', fontsize = 16)
    plt.ylabel(r'$\epsilon_{11}$ ', fontsize = 16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.plot(x,strain11,marker='o');
    plt.grid(color = 'grey', linestyle = '--', linewidth = 0.5)
    plt.show()

if __name__ == '__main__':
    test_isotropic()
