# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project

#-----------------------------------------------------------------------------------------------------------------------#
# Profile.py - Python file of where time analyis is performed.
#-----------------------------------------------------------------------------------------------------------------------#

from time import perf_counter
import cProfile
import pstats
import time
from io import StringIO
from pstats import SortKey
from Material_parameters import *
import Unit_tests
import Test_for_FEM
import test_case_isotropic
import test_case_stress_patch
import test_displacement_patch
import test_case_linear_elastic
import test_anisotropic
import test_rigid_body_translation
import main_routine_
#import Input

# code taken from https://stackoverflow.com/
def writer(tex):
    stream = StringIO()
    stats = pstats.Stats(tex, stream=stream);
    stats.print_stats()
    stream.seek(0)
    print(10*'=',"log.txt file has been generated in the same directory",10*'=')
    data = stream.read()
    return data


# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F1():
    prof.enable()
    Unit_tests.test_hill_criteria()
    prof.disable()
    prof.dump_stats("F1.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F2():
    prof.enable()
    Unit_tests.test_first_derivative_yield_function()
    prof.disable()
    prof.dump_stats("F2.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F3():
    prof.enable()
    Unit_tests.test_second_derivative_yield_function()
    prof.disable()
    prof.dump_stats("F3.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F4():
    prof.enable()
    Unit_tests.test_symmetric_double_der()
    prof.disable()
    prof.dump_stats("F4.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F5():
    prof.enable()
    Unit_tests.test_node_element_list()
    prof.disable()
    prof.dump_stats("F5.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F6():
    prof.enable()
    test_anisotropic.test_hill()
    prof.disable()
    prof.dump_stats("F6.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F7():
    prof.enable()
    test_case_linear_elastic.test_linear_elastic()
    prof.disable()
    prof.dump_stats("F7.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F8():
    prof.enable()
    test_case_stress_patch.stress_patch_test()
    prof.disable()
    prof.dump_stats("F8.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F9():
    prof.enable()
    test_case_isotropic.test_isotropic()
    prof.disable()
    prof.dump_stats("F9.prof")
    
# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F10():
    prof.enable()
    test_displacement_patch.displacement_patch_test()
    prof.disable()
    prof.dump_stats("F10.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F11():
    prof.enable()
    test_rigid_body_translation.rigid_body_translation()
    prof.disable()
    prof.dump_stats("F11.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F12():
    prof.enable()
    Test_for_FEM.test_stiffness_matrix_singularity()
    prof.disable()
    prof.dump_stats("F12.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F13():
    prof.enable()
    Test_for_FEM.test_rigid_body_constraint()
    prof.disable()
    prof.dump_stats("F13.prof")
    
# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F14():
    prof.enable()
    Test_for_FEM.test_rotated_quad_determinant_jacobian()
    prof.disable()
    prof.dump_stats("F14.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F15():
    prof.enable()
    Test_for_FEM.test_quad_der_shape_function()
    prof.disable()
    prof.dump_stats("F15.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F16():
    prof.enable()
    Test_for_FEM.test_rotated_determinant_jacobian()
    prof.disable()
    prof.dump_stats("F16.prof")

# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F17():
    prof.enable()
    Test_for_FEM.test_derivative_shape_function_sum()
    prof.disable()
    prof.dump_stats("F17.prof")


# code taken from https://stackoverflow.com/
prof = cProfile.Profile()
def F18():
    prof.enable()
    main_routine_.main_routine()
    prof.disable()
    prof.dump_stats("F18.prof")
    
# code taken from https://stackoverflow.com/   
prof = cProfile.Profile()
def F19():
    prof.enable()
    Test_for_FEM.test_shape_function_sum()
    prof.disable()
    prof.dump_stats("F19.prof")
    
# code taken from https://stackoverflow.com/
def F20():
    prof.enable()
    Test_for_FEM.test_quad_shape_functions_sum()
    prof.disable()
    prof.dump_stats("F20.prof")




files = ['F1.prof','F2.prof','F3.prof','F4.prof','F5.prof','F6.prof','F7.prof','F8.prof','F9.prof',
        'F10.prof','F11.prof','F12.prof','F13.prof','F14.prof','F15.prof','F16.prof','F17.prof',
        'F18.prof','F19.prof','F20.prof']

Func_s = ['test_hill_criteria', 'test_first_derivative_yield_function', 'test_second_derivative_yield_function', 'test_symmetric_double_der',
          'test_node_element_list', 'test_hill', 'test_linear_elastic', 'stress_patch_test',
          'test_isotropic', 'displacement_patch_test', 'rigid_body_translation', 'test_stiffness_matrix_singularity',
          'test_rigid_body_constraint', 'test_rotated_quad_determinant_jacobian', 'test_quad_der_shape_function', 'test_rotated_determinant_jacobian',
          'test_derivative_shape_function_sum', 'main_routine','test_shape_function_sum','test_quad_shape_functions_sum']

x = []
F1(); F2(); F3(); F4(); F5(); 
F6(); F7();
F8(); F9(); F10(); F11(); F12(); F13();
F14(); F15(); F16(); F17(); F18();
F19();F20()

print("")
print(70*'=')
print(f"               TIME ANALYSIS OF ALL THE {len(files)} TEST FUNCTIONS               ")
print(70*'=')
print("")
for i in files:
    D = writer(i)
    x.append(D)

with open('log.txt', 'w') as myfile:
    myfile.write(50*'=')
    myfile.write("\n")

    for i,j in zip(x,Func_s):
        myfile.write(f"=========={j}==========")
        myfile.write("\n")
        myfile.write(i)
        myfile.write(50*'=')
        myfile.write("\n")


import matplotlib.pyplot as plt
import numpy as np

values = np.array([0.0001,0.0001,0.001,0.001,0.002,1.527,2.615,2.619,6.495,6.550,6.553,6.558,6.561,7.184,7.189,7.353,7.354,254.537])
# =============================================================================
# mylabels =['test_hill_criteria', 'test_first_derivative_yield_function', 'test_second_derivative_yield_function', 'test_symmetric_double_der',
#           'test_node_element_list', 'test_hill', 'test_linear_elastic', 'stress_patch_test',
#           'test_isotropic', 'displacement_patch_test', 'rigid_body_translation', 'test_stiffness_matrix_singularity',
#           'test_rigid_body_constraint', 'test_rotated_quad_determinant_jacobian', 'test_quad_der_shape_function', 'test_rotated_determinant_jacobian',
#           'test_derivative_shape_function_sum', 'main_routine']
# =============================================================================

explode = (0.2, 0.2, 0.2, 0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.4)
fig, ax = plt.subplots()
ax.pie(values, explode=explode,autopct='%1.1f%%', startangle=360)
#plt.pie(values, labels = mylabels)
ax.legend(Func_s, loc='center left', bbox_to_anchor=(1, 0.5))
ax.set_title('Time Analysis')
#plt.legend(title = "Main routine:")
plt.show() 
fig.savefig("Time_analysis_chart.pdf",dpi=200)
















