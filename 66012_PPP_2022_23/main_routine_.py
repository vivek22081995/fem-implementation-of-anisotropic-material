# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project

import numpy as np
import math
from Functions import *
from Material_parameters import * 
from Material_routine_Hill import *
from Element_routine_nonlinear import *
import matplotlib.pyplot as plt
import matplotlib.gridspec as gp
from tqdm import tqdm

def main_routine():
    """      
    Main routine:This is the code needs to be run.All the parametrs are defined already

    In this code the the Element routine  and Material routine is called and final 
    assembly is implemented for 2*2*2 elements

    """
    #Material parameters to define Anisotropic elasticity matrix 
    # =============================================================================
    E1=95238
    E2=95238
    E3=103700
    g=39000
    v=0.3
    
    C = np.zeros((6,6))
    
    C[0,0]=C[1,1]=E1
    C[2,2]=E3
    C[3,3]=C[4,4]=C[5,5]=g
    C[0,1]=C[2,1]=v/E2
    C[0,2]=C[1,2]=-v/E3
    C[2,0]=C[1,0]=-v/E1
    
    t_tot=0.25          #total time for simulation
    delta_T=0.0025       #time increment in seconds
    time_inc = int(t_tot/delta_T)
    #print
    nelx=2              #Number of elements in X-direction
    nely=2             #Number of elements in Y-direction
    nelz=2              #Number of elements in Z-direction
    L=5                 #length
    W=5                 #width
    H=5                 #Height
    F=3000          #force
    
    #Initializing state variables############
    eff_plastic_strain=0
    back_stress1=np.zeros([6,1])
    back_stress2=np.zeros([6,1])
    back_stress3=np.zeros([6,1])
    drag_stress=0
    step=1
    
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)    #element and node list is defined here
    #print(connectivity)
    conn=np.array(connectivity,dtype=int)
    con=conn.tolist()
    n_element=nelx*nely*nelz
    NON=(nelx+1)*(nely+1)*(nelz+1)
    
    u_k_list =[np.zeros([3*NON,1]).tolist()]
    
    epl_list =[np.zeros([6,1]).tolist()]
    strain_list_final= [np.zeros((8,6))]
    stress_list_final=[np.zeros((8,6))]
    F_ext=np.zeros([3*NON,1])
    
    
    for i in tqdm(range(time_inc)):
        #print(i)
        u_k=u_k_list[-1]
        epl = epl_list[-1]
        #print(epl)
        
        error_d_u_k = 10
        error_u_k = 10
        error_R_k = 10
        error_F_int = 10
        
        k_iter=1
        
        F_ext[54]= F_ext[57]=F_ext[60]=F_ext[63]=F_ext[66]= F_ext[69]=F_ext[72]=F_ext[75]=F_ext[78]=F*(i+1)/time_inc
        
        #print(F_ext)
        flag=True
        
        while (error_R_k > 0.005*error_F_int) or (error_d_u_k > 0.005*error_u_k):
            
            K_t=np.zeros([3*NON,3*NON])
            F_int=np.zeros([3*NON,1])
         
            for en in range(n_element):
                con=np.array(connectivity[en],dtype=int)
                con_list=con.tolist()
                u_e=np.zeros([24,1])
                
                i=0
                for e in con_list:
                    u_e[3*i:3*(i+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
                    i+=1
                    
                stress_list_new,strain_list_new,K_ele,F_int_ele,epl_new,back_stress_new1,back_stress_new2,back_stress_new3,drag_stress_new,epl_new,eff_plastic_strain_new=element_routine(C,en,connectivity,coord,u_e,epl,back_stress1,back_stress2,back_stress3,drag_stress,eff_plastic_strain,step,delta_T)
                
                
                
                #assembly of global F_int and Stiffness Matrix
                for i in range(len(con_list)):
                    for j in range(len(con_list)):
                        K_t[3*(int(con_list[i])-1):3*int((con_list[i])),3*(int(con_list[j])-1):3*(int(con_list[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
                        
                        
                for k in range(len(con_list)):
                    F_int[3*(con_list[k]-1):3*con_list[k]]+=F_int_ele[3*k:3*(k+1)].reshape((3,1))
            
                    
                    
            #Residual Calculation
            R_k=F_int-F_ext
            
            #Reduced Residual,displacement and K_t
            F_int_red=np.delete(F_int,(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26),0)
            R_k_red=np.delete(R_k,(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26),0)
            u_k_red=np.delete(u_k,(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26),0)
            K_t_red=np.delete(np.delete(K_t,(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26),0),(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26),1)
    
            #newton Raphson Implementation
            u_k_next_red=np.array(u_k_red)-(np.linalg.inv(K_t_red)@R_k_red) 
            u_k_updated=np.insert(u_k_next_red,(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),0,axis=0)
            
            
            
            
            error_R_k = np.linalg.norm(R_k_red,np.inf)      # reduced vector for error calc
            error_F_int = np.linalg.norm(F_int_red,np.inf)  # reduced vector for error calc
            
            delta_u_k = u_k_updated - u_k
            error_d_u_k =  np.linalg.norm(delta_u_k,np.inf)
            error_u_k = np.linalg.norm(u_k_updated,np.inf)
            
            u_k=u_k_updated
            epl=epl_new
    
        u_k_list.append(u_k_updated)
        strain_list_final.append(strain_list_new)
        stress_list_final.append(stress_list_new)
        epl_list.append(epl_new)
    
    #plotting the initial and final displacements
    u_k_list_final=u_k_list[-1]
    new_displacement=u_k_list_final.reshape((27, 3))
    initial_coords = coord
    final_coords = coord + new_displacement
    
    # Initialize the figure
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    # Loop through the elements and plot the initial and final displacements
    for i in range(conn.shape[0]):
        element_nodes = conn[i]
        initial_disp = np.zeros((8, 3))
        final_disp = np.zeros((8, 3))
        for j in range(0,8):
            node_id = element_nodes[j]
            initial_disp[j] = initial_coords[node_id-1]
            final_disp[j] = final_coords[node_id-1]
        ax.scatter(initial_disp[:,0], initial_disp[:,1], initial_disp[:,2], c='b')
        ax.scatter(final_disp[:,0], final_disp[:,1], final_disp[:,2], c='r')
        for j in range(8):
            ax.plot([initial_disp[j,0], final_disp[j,0]], [initial_disp[j,1], final_disp[j,1]], [initial_disp[j,2], final_disp[j,2]], c='g')
    
    # Set axis labels
    ax.set_xlabel('Y')
    ax.set_ylabel('X')
    ax.set_zlabel('Z')
    #fig.savefig("Nodal_displacements.pdf",dpi=200)
    
    #plotting stress v/s strain
    fig1, ax = plt.subplots()
    fig1.set_size_inches(16,9)
    ax.plot(np.array(strain_list_final)[:,4,0],np.array(stress_list_final)[:,4,0], label = r'$\sigma_{11}$')
    ax.set_xlabel(r'$\epsilon_{11}$', fontsize = 16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.ticklabel_format(axis = 'x',style = 'sci',scilimits = (-3,-3))
    ax.set_ylabel(r'$\sigma{11}$ in MPa',fontsize = 16)
    ax.legend(loc=5,prop={'size': 12})
    plt.grid(color = 'grey', linestyle = '--', linewidth = 0.5)
    plt.show()
    #fig1.savefig("Stress_Strain.pdf",dpi=200)



if __name__ == '__main__':
    main_routine()