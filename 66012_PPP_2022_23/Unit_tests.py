# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# Unit_test.py - Python test file for unit testing of functions used in the program
#-----------------------------------------------------------------------------------------------------------------------#


import numpy as np
from Functions import *

def test_hill_criteria():
    '''
    UNIT TESTING
    Aim : Validity of calculation of the Hill criterion value for a given equivalent stress and N-Matrix

    Expected result :The value of hill_criteria for eq_stress = [1,2,3,4,5,6] and N_matrix =[[1,1,1,0,0,0],
                                                                                             [1,1,1,0,0,0],
                                                                                             [1,1,1,0,0,0],
                                                                                             [0,0,0,1,0,0],
                                                                                             [0,0,0,0,1,0],
                                                                                             [0,0,0,0,0,1]] is 10.63015 

    Test command : pytest Unit_tests.py::test_hill_criteria

    Remarks : Test case passed successfully
    '''
    eq_stress=np.array([1,2,3,4,5,6])
    
    N_matrix=np.array([[1,1,1,0,0,0],[1,1,1,0,0,0],[1,1,1,0,0,0],[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]])
   
    calc_result=np.round(np.sqrt(np.matmul(eq_stress.T,N_matrix@eq_stress)),5)
    
    expected_result=10.63015
    
    flag=0
    if calc_result==expected_result:
       flag=1
    assert(flag==1) is True
    
    
    
def test_first_derivative_yield_function():
    
    '''
    UNIT TESTING
    Aim : Validity of calculation of the first derivative of yield function value for a given equivalent stress and N-Matrix

    Expected result : The value of first derivative of yield function for eq_stress = [1,2,3,4,5,6] and N_matrix =[[1,1,1,0,0,0],
                                                                                                                   [1,1,1,0,0,0],
                                                                                                                   [1,1,1,0,0,0],
                                                                                                                   [0,0,0,1,0,0],
                                                                                                                   [0,0,0,0,1,0],
                                                                                                                   [0,0,0,0,0,1]] is same as expected result

    Test command : pytest Unit_tests.py::test_first_derivative_yield_function

    Remarks : Test case passed successfully
    '''
    
    eq_stress=np.array([1,2,3,4,5,6])
    
    N_matrix=np.array([[1,1,1,0,0,0],[1,1,1,0,0,0],[1,1,1,0,0,0],[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]])
    
    numerator=N_matrix@eq_stress
    
    denominator=np.round(np.sqrt(np.matmul(eq_stress.T,N_matrix@eq_stress)),5)
    calc_result=np.round([numerator/denominator],4)
    
    expected_result=np.array([[0.5644,0.5644,0.5644,0.3763,0.4704,0.5644]])
    
    assert (np.array_equal(expected_result,calc_result)) is True
    

def test_second_derivative_yield_function():
    
    '''
    UNIT TESTING
    Aim : Validity of calculation of the second derivative of yield function value for a given equivalent stress and N-Matrix

    Expected result : The value of second derivative of yield function for eq_stress = [1,2,3,4,5,6] and N_matrix =[[1,1,1,0,0,0],
                                                                                                                   [1,1,1,0,0,0],
                                                                                                                   [1,1,1,0,0,0],
                                                                                                                   [0,0,0,1,0,0],
                                                                                                                   [0,0,0,0,1,0],
                                                                                                                   [0,0,0,0,0,1]] is same as expected result

    Test command : pytest Unit_tests.py::test_second_derivative_yield_function

    Remarks : Test case passed successfully
    '''    

    eq_stress=np.array([1,2,3,4,5,6])
    
    N_mat=np.array([[1,1,1,0,0,0],[1,1,1,0,0,0],[1,1,1,0,0,0],[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]])
    
    term1=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))*N_mat
    term2=np.matmul(N_mat,eq_stress)
    term3=np.matmul(term2,term2)
    numerator=term1-term3
    term5=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    denominator=term5**(3/2)
    calc_result=np.round(numerator/denominator,4)
    expected_result=np.array([[-0.0599,-0.0599,-0.0599,-0.154,-0.154,-0.154],\
                               [-0.0599,-0.0599,-0.0599,-0.154,-0.154,-0.154],\
                               [-0.0599,-0.0599,-0.0599,-0.154,-0.154,-0.154 ],\
                               [-0.154,-0.154,-0.154,-0.0599,-0.154,-0.154 ],\
                               [-0.154,-0.154,-0.154,-0.154,-0.0599,-0.154 ],\
                               [-0.154,-0.154,-0.154,-0.154,-0.154,-0.0599]])
        
    assert (np.array_equal(expected_result,calc_result)) is True
    
def test_symmetric_double_der():
    
    '''
    UNIT TESTING
    Aim : Validity of symmetric of the second derivative of yield function value for a given equivalent stress and N-Matrix

    Expected result : The value of second derivative of yield function for eq_stress = [1,2,3,4,5,6] and N_matrix =[[1,1,1,0,0,0],
                                                                                                                   [1,1,1,0,0,0],
                                                                                                                   [1,1,1,0,0,0],
                                                                                                                   [0,0,0,1,0,0],
                                                                                                                   [0,0,0,0,1,0],
                                                                                                                   [0,0,0,0,0,1]] is symmetric

    Test command : pytest Unit_tests.py::test_symmetric_double_der

    Remarks : Test case passed successfully
    '''    
    eq_stress=np.array([1,2,3,4,5,6])
       
    N_mat=np.array([[1,1,1,0,0,0],[1,1,1,0,0,0],[1,1,1,0,0,0],[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]])
    
    df=dfd_stress(eq_stress,N_mat)
    rtol=1e-05 
    atol=1e-08
    symmetric=np.allclose(df, df.T, rtol=rtol, atol=atol) 
    flag=0
    if symmetric==True:
        flag=1
    assert(flag==1) is True


def test_node_element_list(): 
    '''
    UNIT TESTING
    Aim : Validity of calculation of the element list and node coordinates of a given number of 
          elements on x,y,z direction and Length,Width ,height of an element

    Expected result : The value of element list for nelx=1,nely=1,nelz=1 and node cooedinates for L=1,W=1,H=1 is same as expected element list and node coordinates 

    Test command : pytest Unit_tests.py::test_node_element_list

    Remarks : Test case passed successfully
    '''    
    nelx=1
    nely=1
    nelz=1
    L=1
    W=1
    H=1
    
    
    NOE=nelx*nely*nelz   #Number of elements
    k=0
    connectivity=np.zeros((NOE,8))
    for i in range(1,NOE+1):
        elx=np.fix((i-1)/(nely*nelz))+1
        ely=np.fix(((i-(elx-1)*nely*nelz)-1)/nelz)+1
        elz=i%nelz
        if elz==0:
            elz=nelz
        n1=((nely+1)*(nelz+1)*(elx-1))+((ely-1)*(nelz+1))+elz
        n2=n1+((nely+1)*(nelz+1))
        n3=n2+(nelz+1)
        n4=n1+(nelz+1)
        n5=n1+1
        n6=n2+1
        n7=n3+1
        n8=n4+1
        connectivity[k,:]=[n1,n2,n3,n4,n5,n6,n7,n8]
        k+=1
        
        
    NON=(nelx+1)*(nely+1)*(nelz+1)   #Number of nodes
    coord=np.zeros((NON,3))
    dx=L/nelx
    dy=W/nely
    dz=H/nelz
    m=0
    for ii in range(1,NON+1):
        nx=np.fix((ii-1)/((nely+1)*(nelz+1)))+1
        ny=np.fix((((ii-(nx-1)*(nely+1)*(nelz+1))-1)/(nelz+1)))+1
        nz=(ii)%(nelz+1)
        if nz==0:
            nz=nelz+1
        coord[m,:]=[(nx-1)*dx,(ny-1)*dy,(nz-1)*dz];
        m+=1
    expected_Element_list=np.array([[1,5,7,3,2,6,8,4]])
    expected_coordinate=np.array([[0,0,0],\
                                [0,0,1],\
                                [0,1,0],\
                                [0,1,1],\
                                [1,0,0],\
                                [1,0,1],\
                                [1,1,0],\
                                [1,1,1]])
        
    assert np.array_equal(expected_Element_list,connectivity) and np.array_equal(expected_coordinate,coord) is True


##############################################################################################################################
''' All the functions listed below are same as the functions used in the main program expect for the slight modification done 
    for each of the separate unit tests. Explanations given in the corresponding function files.
'''
###############################################################################################################################

def dfd_stress(N_mat,eq_stress):
    
    term1=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    term2=term1*N_mat
    term3=np.matmul(N_mat,eq_stress)
    term4=np.dot(term3,term3.T)
    numerator=term2-term4
    term5=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    denominator=term5**(3/2)
    dfd_stress=numerator/denominator
    return dfd_stress

