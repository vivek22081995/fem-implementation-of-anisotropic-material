# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# material_routine_Hill.py - Python file of material routine of anisotropic Hill48 yield criteria
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np
import math
from Functions import *
from Material_parameters import *


def material_routine(C,strain_prev,eps,back_stress1,back_stress2,back_stress3,drag_stress,eff_plastic_strain,step,delta_T):
    '''
    This function calculates elasto-plastic tangent and new stresses and new plastic strain using closest point projection algorithm of Hill48 yield criteria

    INPUTS :
        C                   : ndarray(6*6)
                              elasticity matrix
        strain_prev         : ndarray(6*1)
                              scalar value 
        eps                 : ndarray(6*1)
                              plastic strain 
        back_stress1        : ndarray(6*1)
                              back stress component number 1
        back_stress2        : ndarray(6*1)
                              back stress component number 2
        back_stress3        : ndarray(6*1)
                              back stress component number 3
        drag_stress         : scalar value
                              drag stress 
       eff_plastic_strain   : scalar value
                              effective plastic strain
        step                : scalar value
                              number of steps
        
    RETURNS :
        C_ep                : ndarray(6*6)
                              elasto-plastic tangent matrix
        stress_new          : ndarray(6*1)
                              new stress value
        plastic_strain_new  : ndarray
                              List of the node numbers of all the elements
        back_stress_new1    : ndarray
                              new back stress component number 1
        back_stress_new1    : ndarray
                              new back stress component number 2
        back_stress_new1    : ndarray
                              new back stress component number 3
        drag_stress_new     : scalar value
                              new drag stress
        eff_plastic_strain_new : scalar value
                              new effective plastic strain value                     
    '''
    material_parameter = input_parameter();
    v=material_parameter[0]             # Poisson's ration          
    y0=material_parameter[1]            # Initial Yield stress
    c=material_parameter[2]             #back stress constant
    a=material_parameter[3]             #back stress constant(a)
    R_s=material_parameter[4]           #maximum isotropic hardening
    b=material_parameter[5]             #rate of isotropic hardening
    a1=material_parameter[6]            #back stress constant(c1)
    a2=material_parameter[7]            #back stress constant(c2)
    a3=material_parameter[8]            #back stress constant(c3)
    c1=material_parameter[9]            #back stress constant(a1)
    c2=material_parameter[10]           #back stress constant(a2)
    c3=material_parameter[11]           #back stress constant(a3)
    
    #delta_T=0.0025
    
    eff_plastic_strain=eff_plastic_strain
    
    drag_stress=drag_stress
    back_stress1=back_stress1
    back_stress2=back_stress2
    back_stress3=back_stress3
    back_stress=back_stress1+back_stress2+back_stress3
    
    Identity_matrix=np.identity(6)
    
    
    #########Ansiotopy constants#############
    F=0.5   
    G=0.5
    H=0.537
    L=1.367
    M=N=1.5
    
    N_mat=np.zeros([6,6],dtype=float)
    N_mat[0,0]=G+H
    N_mat[1,1]=F+H
    N_mat[2,2]=F+G
    N_mat[3,3]=2*L
    N_mat[4,4]=2*M
    N_mat[5,5]=2*N
    N_mat[0,1]=N_mat[1,0]=-H
    N_mat[0,2]=N_mat[2,0]=-G
    N_mat[1,2]=N_mat[2,1]=-F

    
    total_strain=strain_prev         #Total strain tensor
       
    plastic_strain=eps 
    
    elastic_strain=np.empty((6,1))   #elastic strain tensor
    
    
    for i in range(0,6):  
        elastic_strain[i]=total_strain[i]-plastic_strain[i] 
    
    trial_stress=np.matmul(C,elastic_strain)
    
    eq_stress=trial_stress-back_stress
    
    hill_criteria=hill(eq_stress,N_mat)[0,0]
    
    yield_func=hill_criteria-(y0+drag_stress)
    
    
    if yield_func<0:
        stress_new=trial_stress
        C_ep=C
        plastic_strain_new=eps
        back_stress_new1=back_stress1
        back_stress_new2=back_stress2
        back_stress_new3=back_stress3
        drag_stress_new=drag_stress
        eff_plastic_strain_new=eff_plastic_strain
    
    else:
        #first derivative of yield function
        b_init=df_stress(N_mat,eq_stress)
        
        #hardening modulus
       
        H_new=(-c*a*(2/3)*np.matmul(b_init.T,b_init))+((-np.sqrt(2/3)*R_s*b*math.exp(-b*eff_plastic_strain)*np.sqrt(np.matmul(b_init.T,b_init))))
        #plastic multiplier
        lambda_init=(yield_func)/((np.matmul(b_init.T,np.matmul(C,b_init))-H_new))[0,0]
        
        #change in plastic straain
        chg_plastic_strain_new=lambda_init*b_init
        
        #change in effective plastic strain
        chg_eff_plastic_strain_new=np.sqrt((2/3)*np.matmul(chg_plastic_strain_new.T,chg_plastic_strain_new))[0,0]
        
        # 3 component back stress tensor
        back_stress_new1=(back_stress1+(c1*a1*(2/3)*chg_plastic_strain_new))/(1+c1*chg_eff_plastic_strain_new)
        back_stress_new2=(back_stress2+(c2*a2*(2/3)*chg_plastic_strain_new))/(1+c2*chg_eff_plastic_strain_new)
        back_stress_new3=(back_stress3+(c3*a3*(2/3)*chg_plastic_strain_new))/(1+c3*chg_eff_plastic_strain_new)
        back_stress_new=back_stress_new1+back_stress_new2+back_stress_new3
        
        eff_plastic_strain_new=eff_plastic_strain+chg_eff_plastic_strain_new
        
        #drag stress
        drag_stress_new=drag_stress+delta_T*(R_s*(1-math.exp(-b*eff_plastic_strain_new)))
        
        #new stress
        stress_new=trial_stress-(lambda_init*np.matmul(C,b_init))
        
        #new equivalent stress
        eq_stress_new=stress_new-back_stress_new
        
        TolR=10**-5
        TolF=10**-5
        
        flag=True
        while flag:
            
            hill_criteria_new=hill(eq_stress_new,N_mat)[0,0]
            
            yield_func_new=hill_criteria_new-(y0+drag_stress_new)
            #print(yield_func_new)
            
            b_new=df_stress(N_mat,eq_stress_new)
            
            #residual is calculated
            residual=stress_new-(trial_stress-(lambda_init*np.matmul(C,b_new)))
            
            #double derivative of yield function
            D_new=dfd_stress(N_mat,eq_stress_new)
            
            Q=Identity_matrix+(lambda_init*np.dot(C,D_new))
            
            Q_inverse=inverse_matrix(Q)
            
            R=np.dot(Q_inverse,C)
            
            residual_norm=np.linalg.norm(residual)
            #print(residual_norm)
            
            if (residual_norm < TolR) and (yield_func_new<TolF):
                break
        
            H_new=(-c*a*(2/3)*np.matmul(b_new.T,b_new))+((-np.sqrt(2/3)*R_s*b*math.exp(-b*eff_plastic_strain_new)*np.sqrt(np.matmul(b_new.T,b_new))))
           
            
            num=(yield_func_new-(np.matmul(np.matmul(b_new.T,Q_inverse),residual)))[0,0]
            denom=(np.matmul(np.matmul(b_new.T,R),b_new))-H_new
            lambda_dot=num/denom[0,0]
            
            lambda_init+=lambda_dot
            
            stress_dot1=residual+lambda_dot*np.matmul(C,b_new)
            stress_dot=np.matmul(-Q_inverse,stress_dot1)
            
            chg_plastic_strain_new=lambda_dot*b_new
            
            chg_eff_plastic_strain_new=np.sqrt((2/3)*np.matmul(chg_plastic_strain_new.T,chg_plastic_strain_new))[0,0]
            
            back_stress_new1=(back_stress_new1+(c1*a1*(2/3)*chg_plastic_strain_new))/(1+c1*chg_eff_plastic_strain_new)
            back_stress_new2=(back_stress_new2+(c2*a2*(2/3)*chg_plastic_strain_new))/(1+c2*chg_eff_plastic_strain_new)
            back_stress_new3=(back_stress_new3+(c3*a3*(2/3)*chg_plastic_strain_new))/(1+c3*chg_eff_plastic_strain_new)
            back_stress_new=back_stress_new1+back_stress_new2+back_stress_new3
        
            eff_plastic_strain_new=eff_plastic_strain+chg_eff_plastic_strain_new
        
            drag_stress_new=drag_stress_new+delta_T*(R_s*(1-math.exp(-b*eff_plastic_strain_new)))
            
            
            stress_new+=stress_dot
            
            eq_stress_new=stress_new-back_stress_new
            
        #new plastic strain is calculated
        plastic_strain_new=plastic_strain+chg_plastic_strain_new
        
        #consisten tangent modular matrix is calculated here
        term1=np.matmul(R,b_new)
        term2=np.matmul(b_new.T,R)
        numerator=np.matmul(term1,term2)
        term3=np.matmul(term2,b_new)
        denominator=term3-H_new
        C_ep=R-(numerator/denominator)
        
    return C_ep,stress_new,back_stress_new1,back_stress_new2,back_stress_new3,drag_stress_new,plastic_strain_new,eff_plastic_strain_new
            
##############################################################################################################################
''' All the functions listed below are same as the functions used in the main program  
'''
###############################################################################################################################        
    
    
def hill(eq_stress,N_mat):
    term=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    hill=np.sqrt(term)
    return hill


def df_stress(N_mat,eq_stress):
    
    term1=np.matmul(N_mat,eq_stress)
    term2=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    term3=np.sqrt(term2)
    df_stress=term1/term3
    return df_stress


def dfd_stress(N_mat,eq_stress):

    term1=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    term2=term1*N_mat
    term3=np.matmul(N_mat,eq_stress)
    term4=np.dot(term3,term3.T)
    numerator=term2-term4
    term5=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    denominator=term5**(3/2)
    dfd_stress=numerator/denominator
    return dfd_stress    
