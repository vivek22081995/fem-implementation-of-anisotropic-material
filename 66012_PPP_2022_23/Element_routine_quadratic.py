# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# Element_routine_quadratic.py - Python  file for quadratic element routine for one element 
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np

coord=np.array([[-1,-1,-1],[-1,-1,0],[-1,-1,1],[0,-1,1],[1,-1,1],[1,-1,0],[1,-1,-1],[0,-1,-1],[-1,0,-1],[-1,0,1],[1,0,1],[1,0,-1],\
                [-1,1,-1],[-1,1,0],[-1,1,1],[0,1,1],[1,1,1],[1,1,0],[1,1,-1],[0,1,-1]])
    
def element_routine_quadratic(coord,C,u_e):
    '''
    This function calculates elemental stiffnesss matrix, F_int element ,stress and strain for each element using given displacements.

    INPUTS :
        C                   : ndarray
                              6*6 elatsiyity matrix
    
        coord               : ndarray
                              List of coordinates of all nodes of an element
        u_e                 : ndarray(24*1)
                              List of displacements
        
    RETURNS :
        stress_list         : list
                              Updated list of stress of every gauss points
        strain_list         : list
                              Updated list of stress of every gauss points
        K_ele               : 60*60 matrix
                              Elemental stiffness matrix of each element
        F_int_ele           : 60*1 array
                              Elemental F_int of each element     
    '''
    stress_list=np.zeros((27,6))
    strain_list=np.zeros((27,6))
    
    K_ele=np.zeros([60,60])
        
    F_int_ele=np.zeros([60,1])
    
    for i in [-0.774596669241483, 0.0, 0.774596669241483]:
        for j in [-0.774596669241483, 0.0, 0.774596669241483]:
            for k in [-0.774596669241483, 0.0, 0.774596669241483]:
                if i==0.0 or j==0.0 or k==0.0:
                    w=0.888888888888889
                else:
                    w=0.555555555555556
                deriv_N=der_quad_shape_functions(i, j, k)
                Jacobian_matrix=deriv_N@coord
                detJ=np.linalg.det(Jacobian_matrix)
                
                Jacobian_inverse=np.linalg.inv(Jacobian_matrix)
                
                
                Nxyz=Jacobian_inverse@deriv_N
                
                N1x = Nxyz[0,0]; N2x = Nxyz[0,1]; N3x = Nxyz[0,2]; N4x = Nxyz[0,3]; N5x = Nxyz[0,4]; N6x = Nxyz[0,5]; N7x = Nxyz[0,6]; 
                N8x = Nxyz[0,7]; N9x = Nxyz[0,8]; N10x= Nxyz[0,9]; N11x= Nxyz[0,10];N12x=Nxyz[0,11]; N13x=Nxyz[0,12]; N14x=Nxyz[0,13];
                N15x=Nxyz[0,14]; N16x=Nxyz[0,15]; N17x=Nxyz[0,16]; N18x= Nxyz[0,17];N19x=Nxyz[0,18]; N20x=Nxyz[0,19]
                
                N1y = Nxyz[1,0]; N2y = Nxyz[1,1]; N3y = Nxyz[1,2]; N4y = Nxyz[1,3]; N5y = Nxyz[1,4]; N6y = Nxyz[1,5]; N7y = Nxyz[1,6]; 
                N8y = Nxyz[1,7]; N9y = Nxyz[1,8]; N10y= Nxyz[1,9]; N11y= Nxyz[1,10];N12y=Nxyz[1,11]; N13y=Nxyz[1,12]; N14y=Nxyz[1,13];
                N15y=Nxyz[1,14]; N16y=Nxyz[1,15]; N17y=Nxyz[1,16]; N18y= Nxyz[1,17];N19y=Nxyz[1,18]; N20y=Nxyz[1,19]
                
                N1z = Nxyz[2,0]; N2z = Nxyz[2,1]; N3z = Nxyz[2,2]; N4z = Nxyz[2,3]; N5z = Nxyz[2,4]; N6z = Nxyz[2,5]; N7z = Nxyz[2,6]; 
                N8z = Nxyz[2,7]; N9z = Nxyz[0,8]; N10z= Nxyz[0,9]; N11z= Nxyz[0,10];N12z=Nxyz[0,11]; N13z=Nxyz[0,12]; N14z=Nxyz[0,13];
                N15z=Nxyz[0,14]; N16z=Nxyz[0,15]; N17z=Nxyz[0,16]; N18z= Nxyz[0,17];N19z=Nxyz[0,18]; N20z=Nxyz[0,19]
                
                
                B = np.array([[N1x,0,0,N2x,0,0,N3x,0,0,N4x,0,0,N5x,0,0,N6x,0,0,N7x,0,0,N8x,0,0,N9x,0,0,N10x,0,0,N11x,0,0,N12x,0,0,N13x,0,0,N14x,0,0,N15x,0,0,N16x,0,0,N17x,0,0,N18x,0,0,N19x,0,0,N20x,0,0], \
                                   [0,N1y,0,0,N2y,0,0,N3y,0,0,N4y,0,0,N5y,0,0,N6y,0,0,N7y,0,0,N8y,0,0,N9y,0,0,N10y,0,0,N11y,0,0,N12y,0,0,N13y,0,0,N14y,0,0,N15y,0,0,N16y,0,0,N17y,0,0,N18y,0,0,N19y,0,0,N20y,0], \
                                   [0,0,N1z,0,0,N2z,0,0,N3z,0,0,N4z,0,0,N5z,0,0,N6z,0,0,N7z,0,0,N8z,0,0,N9z,0,0,N10z,0,0,N11z,0,0,N12z,0,0,N13z,0,0,N14z,0,0,N15z,0,0,N16z,0,0,N17z,0,0,N18z,0,0,N19z,0,0,N20z], \
                                   [N1y,N1x,0,N2y,N2x,0,N3y,N3x,0,N4y,N4x,0,N5y,N5x,0,N6y,N6x,0,N7y,N7x,0,N8y,N8x,0,N9y,N9x,0,N10y,N10x,0,N11y,N11x,0,N12y,N12x,0,N13y,N13x,0,N14y,N14x,0,N15y,N15x,0,N16y,N16x,0,N17y,N17x,0,N18y,N18x,0,N19y,N19x,0,N20y,N20x,0], \
                                   [0,N1z,N1y,0,N2z,N2y,0,N3z,N3y,0,N4z,N4y,0,N5z,N5y,0,N6z,N6y,0,N7z,N7y,0,N8z,N8y,0,N9z,N9y,0,N10z,N10y,0,N11z,N11y,0,N12z,N12y,0,N13z,N13y,0,N14z,N14y,0,N15z,N15y,0,N16z,N16y,0,N17z,N17y,0,N18z,N18y,0,N19z,N19y,0,N20z,N20y], \
                                   [N1z,0,N1x,N2z,0,N2x,N3z,0,N3x,N4z,0,N4x,N5z,0,N5x,N6z,0,N6x,N7z,0,N7x, N8z,0, N8x,N9z,0,N9x,N10z,0,N10x,N11z,0,N11x,N12z,0,N12x,N13z,0,N13x,N14z,0,N14x,N15z,0,N15x, N16z,0, N16x,N17z,0,N17x,N18z,0,N18x,N19z,0,N19x,N20z,0, N20x]])
                
                strain=np.matmul(B,u_e)
                strain_list[i,:]=strain.reshape(1,6)
                
                stress=C@strain
                stress_list[i,:]=stress.reshape(1,6)
                
                BD=w*(np.matmul(B.T,np.matmul(C,B)))*detJ
                K_ele=K_ele+BD
                
                F_int_ele=F_int_ele+w*np.matmul(B.T,stress)*detJ

    return K_ele,F_int_ele,stress_list,strain_list


########################################################################################################################

def shape_functions_quadratic(s,t,u):
    
    N1=(1/8)*(1-s)*(1-t)*(1-u)*(-s-t-u-2)
    N2=(1/4)*(1-s)*(1-t)*(1-u**2)
    N3=(1/8)*(1-s)*(1-t)*(1+u)*(-s-t+u-2)
    N4=(1/4)*(1-s**2)*(1-t)*(1+u)
    N5=(1/8)*(1+s)*(1-t)*(1+u)*(s-t+u-2)
    N6=(1/4)*(1+s)*(1-t)*(1-u**2)
    N7=(1/8)*(1+s)*(1-t)*(1-u)*(s-t-u-2)
    N8=(1/4)*(1-s**2)*(1-t)*(1-u)
    N9=(1/4)*(1-s)*(1-t**2)*(1-u)
    N10=(1/4)*(1-s)*(1-t**2)*(1+u)
    N11=(1/4)*(1+s)*(1-t**2)*(1+u)
    N12=(1/4)*(1+s)*(1-t**2)*(1-u)
    N13=(1/8)*(1-s)*(1+t)*(1-u)*(-s+t-u-2)
    N14=(1/4)*(1-s)*(1+t)*(1-u**2)
    N15=(1/8)*(1-s)*(1+t)*(1+u)*(-s+t+u-2)
    N16=(1/4)*(1-s**2)*(1+t)*(1+u)
    N17=(1/8)*(1+s)*(1+t)*(1+u)*(s+t+u-2)
    N18=(1/4)*(1+s)*(1+t)*(1-u**2)
    N19=(1/8)*(1+s)*(1+t)*(1-u)*(s+t-u-2)
    N20=(1/4)*(1-s**2)*(1+t)*(1-u)
        
    return N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20
       
def der_quad_shape_functions(s,t,u):
    
    dN1s=(1/8)*(1-t)*(1-u)*(2*s+t+u+1)       ##############diff wrt s##############
    dN2s=(-1/4)*(1-t)*(1-u**2)
    dN3s=(1/8)*(1-t)*(1+u)*(2*s+t-u+1)
    dN4s=(-1/2)*s*(1-t)*(1+u)
    dN5s=(1/8)*(1-t)*(1+u)*(2*s-t+u-1)
    dN6s=(1/4)*(1-t)*(1-u**2)
    dN7s=(1/8)*(1-t)*(1-u)*(2*s-t-u-1)
    dN8s=(-1/2)*s*(1-t)*(1-u)
    dN9s=(-1/4)*(1-t**2)*(1-u)
    dN10s=(-1/4)*(1-t**2)*(1+u)
    dN11s=(1/4)*(1-t**2)*(1+u)
    dN12s=(1/4)*(1-t**2)*(1-u)
    dN13s=(1/8)*(1+t)*(1-u)*(2*s-t+u+1)
    dN14s=(-1/4)*(1+t)*(1-u**2)
    dN15s=(1/8)*(1+t)*(1+u)*(2*s-t-u+1)
    dN16s=(-1/2)*(s)*(1+t)*(1+u)
    dN17s=(1/8)*(1+t)*(1+u)*(2*s+t+u-1)
    dN18s=(1/4)*(1+t)*(1-u**2)
    dN19s=(1/8)*(1+t)*(1-u)*(2*s+t-u-1)
    dN20s=(-1/2)*(s)*(1+t)*(1-u)
    
    dN1t=(1/8)*(1-s)*(1-u)*(s+2*t+u+1)       ##############diff wrt s##############
    dN2t=(-1/4)*(1-s)*(1-u**2)
    dN3t=(1/8)*(1-s)*(1+u)*(s+2*t-u+1)
    dN4t=(-1/4)*(1-s**2)*(1+u)
    dN5t=(1/8)*(1+s)*(1+u)*(-s+2*t-u+1) 
    dN6t=(-1/4)*(1+s)*(1-u**2)
    dN7t=(1/8)*(1+s)*(1-u)*(-s+2*t+u+1)
    dN8t=(-1/4)*(1-s**2)*(1-u)
    dN9t=(-1/2)*(1-s)*(t)*(1-u)
    dN10t=(-1/2)*(1-s)*(t)*(1+u)
    dN11t=(-1/2)*(1+s)*(t)*(1+u)
    dN12t=(-1/2)*(1+s)*(t)*(1-u)
    dN13t=(1/8)*(1-s)*(1-u)*(-s+2*t-u-1)
    dN14t=(1/4)*(1-s)*(1-u**2)
    dN15t=(1/8)*(1-s)*(1+u)*(-s+2*t+u-1)
    dN16t=(1/4)*(1-s**2)*(1+u)
    dN17t=(1/8)*(1+s)*(1+u)*(s+2*t+u-1)
    dN18t=(1/4)*(1+s)*(1-u**2)
    dN19t=(1/8)*(1+s)*(1-u)*(s+2*t-u-1)
    dN20t=(1/4)*(1-s**2)*(1-u)
    
    dN1u=(1/8)*(1-s)*(1-t)*(s+t+2*u+1)       ##############diff wrt s##############
    dN2u=(-1/2)*(1-s)*(1-t)*(u)
    dN3u=(1/8)*(1-s)*(1-t)*(-s-t+2*u-1)
    dN4u=(1/4)*(1-s**2)*(1-t)
    dN5u=(1/8)*(1+s)*(1-t)*(s-t+2*u-1)
    dN6u=(-1/2)*(1+s)*(1-t)*(u)
    dN7u=(1/8)*(1+s)*(1-t)*(-s+t+2*u+1)
    dN8u=(-1/4)*(1-s**2)*(1-t)
    dN9u=(-1/4)*(1-s)*(1-t**2)
    dN10u=(1/4)*(1-s)*(1-t**2)
    dN11u=(1/4)*(1+s)*(1-t**2)
    dN12u=(-1/4)*(1+s)*(1-t**2)
    dN13u=(1/8)*(1-s)*(1+t)*(s-t+2*u+1)
    dN14u=(-1/2)*(1-s)*(1+t)*(u)
    dN15u=(1/8)*(1-s)*(1+t)*(-s+t+2*u-1)
    dN16u=(1/4)*(1-s**2)*(1+t)
    dN17u=(1/8)*(1+s)*(1+t)*(s+t+2*u-1)
    dN18u=(-1/2)*(1+s)*(1+t)*(u)
    dN19u=(1/8)*(1+s)*(1+t)*(-s-t+2*u+1)
    dN20u=(-1/4)*(1-s**2)*(1+t)
    
    dN_mat=np.array([[dN1s,dN2s,dN3s,dN4s,dN5s,dN6s,dN7s,dN8s,dN9s,dN10s,dN11s,dN12s,dN13s,dN14s,dN15s,dN16s,dN17s,dN18s,dN19s,dN20s],\
                    [dN1t,dN2t,dN3t,dN4t,dN5t,dN6t,dN7t,dN8t,dN9t,dN10t,dN11t,dN12t,dN13t,dN14t,dN15t,dN16t,dN17t,dN18t,dN19t,dN20t],\
                    [dN1u,dN2u,dN3u,dN4u,dN5u,dN6u,dN7u,dN8u,dN9u,dN10u,dN11u,dN12u,dN13u,dN14u,dN15u,dN16u,dN17u,dN18u,dN19u,dN20u]])
    
    return dN_mat


            