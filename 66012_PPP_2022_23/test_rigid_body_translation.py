# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# test_rigid_body_translation.py - Python test file testing rigid body nature of the structure
#-----------------------------------------------------------------------------------------------------------------------#


import numpy as np
from Functions import *
from Element_routine_linear import *

def rigid_body_translation():

    '''
    PATCH TESTING
    
    Aim : Test the rigid body nature of the structure with translation along the  direction. A structure of dimensions 10*10*10 is considered 
          and subjected to a displacement of 2 for each node.

    Expected result : The displacement of the other end  nodes are all equal with each other and having a magnitude equal to that of the displacement 
                      boundary condition applied to the boundary nodes .

    Test Command : test_rigid_body_translation.py::rigid_body_translation

    Remarks : Test case passed successfully
     
    '''    

    E=210e3;
    NU=0.3;
    D= E/((1+NU)*(1-2*NU))*np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                        [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                        [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])
        
    nelx=1      #number of elements in x direction
    nely=1      #number of elements in y direction
    nelz=1      #number of elements in z direction
    L=10         #Length
    W=10         #Width
    H=10         #Height
    
    
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)    #element and node list is defined here
    
    NOE=nelx*nely*nelz                                   #Number of Elements
    NON=(nelx+1)*(nely+1)*(nelz+1)                       #Number of Nodes
    
    u_k_list_new =[np.zeros([3*NON,1]).tolist()]         #Global displacement list
    
    strain_list_final= [np.zeros((8,6))]                 #stress list global
    
    stress_list_final=[np.zeros((8,6))]                  #strain list global
    
    F_ext=np.zeros([3*NON,1])                            #External force global
    
    
    K_t=np.zeros([3*NON,3*NON])                          #Global stiffness Matrix
    
    F_int=np.zeros([3*NON,1])                            #Global Internal force 
    
    for en in range(NOE):
        u_k=np.array(u_k_list_new[-1])
        u_k[12]=u_k[15]=u_k[18]=u_k[21]=2
        
        con=np.array(connectivity[en],dtype=int)
        con_list=con.tolist()
        
        
        u_e=np.zeros([24,1])
        
        i=0
        for e in con_list:
            u_e[3*i:3*(i+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
            i+=1
        
        stress_list,strain_list,K_ele,F_int_ele=element_routine(D,en,connectivity,coord,u_e)
        
        #assembly of global F_int and Stiffness Matrix
        for i in range(len(con_list)):
            for j in range(len(con_list)):
                K_t[3*(int(con_list[i])-1):3*int((con_list[i])),3*(int(con_list[j])-1):3*(int(con_list[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
       
        for k in range(len(con_list)):
            F_int[3*(con_list[k]-1):3*con_list[k]]+=F_int_ele[3*k:3*(k+1)].reshape((3,1))
            
        R_k=F_int-F_ext
        
        R_k_red=np.delete(R_k,(1,2,4,5,7,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23),0)
        K_t_red=np.delete(np.delete(K_t,(1,2,4,5,7,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23),0),(1,2,4,5,7,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23),1)
        
        K_inverse_red=np.linalg.inv(K_t_red)
        delta_u_red =  -K_inverse_red@R_k_red
        
        u_k_updated=np.insert(delta_u_red,(1,1,2,2,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4),0,axis=0)
    u_k_list_new.append(u_k_updated)
    
if __name__ == '__main__':
    rigid_body_translation()