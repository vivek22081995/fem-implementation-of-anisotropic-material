# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# test_case_linear_elastic.py - Python test file testing linear elasticity of FEM implementation
#-----------------------------------------------------------------------------------------------------------------------#


import numpy as np
from Functions import *
from Element_routine_linear import *
import matplotlib.pyplot as plt
from tqdm import tqdm

def test_linear_elastic():
    '''
    TESTING
    Aim : Validity of element routine  for linear elastic behaviour of stress and strain

    Expected result : linear behaviour of stress v/s strain graph

    Test command : test_case_linear_elastic.py::test_linear_elastic

    Remarks : Test case passed successfully
    '''
    
    E=210e3
    NU=0.3
    
    #######Elasticity matrix##########
    D =(E/((1+NU)*(1-2*NU)))* np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                        [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                        [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])
        
    
        
    nelx=2 #number of elements in X-direction
    nely=1 #number of elements in Y-direction
    nelz=1 #number of elements in Z-direction
    L=2    #Length 
    W=2;   #width 
    H=2;   #Height
    
    
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)   # element and node list 
    NOE=nelx*nely*nelz                                  # Number of elements
    NON=(nelx+1)*(nely+1)*(nelz+1)                      # Number of Nodes
    
    t_tot=0.25                                          # total time for Simulation
    delta_T=0.0025                       
    time_inc=int(t_tot/delta_T)                         # time increment in seconds
    F=200
    
    
    u_k_list =[np.zeros([3*NON,1])]    
    
    strain_list_final= [np.zeros((8,6))]
    
    stress_list_final=[np.zeros((8,6))]
    
    F_ext=np.zeros([3*NON,1])                          # Initializing external force
    
    for i in tqdm(range(time_inc)):
        u_k=u_k_list[-1]
        
    
        F_ext[24]= F_ext[27]=F_ext[30]=F_ext[33]=F*(i+1)/time_inc
    
        K_t=np.zeros([3*NON,3*NON])
        F_int=np.zeros([3*NON,1])
        
        for en in range(NOE):
            
            con=np.array(connectivity[en],dtype=int)
            
            u_e=np.zeros([24,1])
            l=0
            for e in con:
                u_e[3*l:3*(l+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
                l+=1
            
            #element routine is called for each element
            stress_list,strain_list,K_ele,F_int_ele=element_routine(D,en,connectivity,coord,u_e)
            
            #Global stiffness matrix assembly
            for i in range(len(con)):
                for j in range(len(con)):
                    K_t[3*(int(con[i])-1):3*int((con[i])),3*(int(con[j])-1):3*(int(con[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
            
        
            #Global F_int assembly
            for k in range(len(con)):
                F_int[3*(con[k]-1):3*con[k]]+=F_int_ele[3*k:3*(k+1)]
            
        R_k=F_int-F_ext
        
        #applying boundary conditions
        R_k_red=np.delete(R_k,(0,1,2,3,4,5,6,7,8,9,10,11),0)
        u_k_red=np.delete(u_k,(0,1,2,3,4,5,6,7,8,9,10,11),0)
        K_t_red=np.delete(np.delete(K_t,(0,1,2,3,4,5,6,7,8,9,10,11),0),(0,1,2,3,4,5,6,7,8,9,10,11),1)
        K_t_inverse_red=np.linalg.inv(K_t_red)
        
        #new displacment is calculated
        delta_uk=K_t_inverse_red@R_k_red
        u_k_next=u_k_red-delta_uk
        
        #new displacement is updated
        u_k_updated=np.insert(u_k_next,(0,0,0,0,0,0,0,0,0,0,0,0),0,axis=0)
        
        u_k_list.append(u_k_updated)
        strain_list_final.append(strain_list)
        stress_list_final.append(stress_list)
    
    #Stress v/s Strain graph is plotted here
    fig1, ax = plt.subplots()
    fig1.set_size_inches(16,9)
    ax.plot(np.array(strain_list_final)[:,4,0],np.array(stress_list_final)[:,4,0], label = r'$\sigma_{11}$')
    ax.set_xlabel(r'$\epsilon_{11}$', fontsize = 16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.ticklabel_format(axis = 'x',style = 'sci',scilimits = (-3,-3))
    ax.set_ylabel(r'$\sigma{11}$ in MPa',fontsize = 16)
    ax.legend(loc=5,prop={'size': 12})
    plt.grid(color = 'grey', linestyle = '--', linewidth = 0.5)
    plt.show()

if __name__ == '__main__':
    test_linear_elastic()