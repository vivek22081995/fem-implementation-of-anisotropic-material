# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# test_case_stress_patch.py - Python test file testing stresses at each gauss point for FEM implementation
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np
from Functions import *
from Element_routine_linear import *


def stress_patch_test():
    '''
    PATCH TESTING
    Aim : A single element is subjected to equilibrium using prescribed displacement in the x direction on the left and right edges.

    Expected result : The stresses and strains at all the eight gauss points are equal.

    Test Command : test_case_stress_patch.py::stress_patch_test

    Remarks : Test case passed successfully
    '''
    
    
    #######Elasticity matrix##########
    E=210e3
    NU=0.3
    D =(E/((1+NU)*(1-2*NU)))* np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                        [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                        [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])
        
    
        
    nelx=2 #number of elements in X-direction
    nely=1 #number of elements in Y-direction
    nelz=1 #number of elements in Z-direction
    L=2    #Length 
    W=2;   #width 
    H=2;   #Height
    
    
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)   # element and node list 
    
    NOE=nelx*nely*nelz                  #Number of elements
    NON=(nelx+1)*(nely+1)*(nelz+1)      #Number of Nodes
    
    u_k_list =[np.zeros([3*NON,1])]    
    
    strain_list_final= [np.zeros((8,6))]
    
    stress_list_final=[np.zeros((8,6))]
    
    u_k=u_k_list[-1]
    
    
    u_k[0]= u_k[3]=u_k[6]=u_k[9]=u_k[24]=u_k[27]=u_k[30]=u_k[33]=2
    
    K_t=np.zeros([3*NON,3*NON])
    F_int=np.zeros([3*NON,1])
    
    for en in range(NOE):
    
        con=np.array(connectivity[en],dtype=int)
        u_e=np.zeros([24,1])
        l=0
        for e in con:
            u_e[3*l:3*(l+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
            l+=1
        
        stress_list,strain_list,K_ele,F_int_ele=element_routine(D,en,connectivity,coord,u_e)
        
        
        for i in range(len(con)):
            for j in range(len(con)):
                K_t[3*(int(con[i])-1):3*int((con[i])),3*(int(con[j])-1):3*(int(con[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
    
        for k in range(len(con)):
            F_int[3*(con[k]-1):3*con[k]]+=F_int_ele[3*k:3*(k+1)]
        
    
    strain_list_final.append(strain_list)
    stress_list_final.append(stress_list)

if __name__ == '__main__':
    stress_patch_test()
