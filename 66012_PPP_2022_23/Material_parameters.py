# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project

#-----------------------------------------------------------------------------------------------------------------------#
# Material_parameters.py - Python file of material parameters used in material routine
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np

def input_parameter():
    
    parameter = np.empty(12)     
    parameter[0] = 0.3;         # Poisson's ration
    parameter[1] = 740.0;       # Initial Yield stress
    parameter[2] = 148          #back stress constant(c)
    parameter[3] = 685         #back stress constant(a)
    parameter[4] = -10          #maximum isotropic hardening(R_s)
    parameter[5] = 20           #rate of isotropic hardening(b)
    parameter[6] = 148          #back stress constant(c1)
    parameter[7] = 253          #back stress constant(c2)
    parameter[8] = 10           #back stress constant(c3)
    parameter[9] = 685          #back stress constant(a1)
    parameter[10] = 130         #back stress constant(a2)
    parameter[11] = 50          #back stress constant(a3)

    return parameter 


