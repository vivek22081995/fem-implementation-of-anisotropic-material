# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# driver_routine_Hill.py - Python file to verify material_routine Hill by performing simple tensile test
#-----------------------------------------------------------------------------------------------------------------------#


import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from Material_routine_Hill import *

def test_hill():
    '''
    Tensile TESTING
    Aim : Verification of the Ansiotropic hill criterion material routine for a simple tensile test

    Expected result : Non linear stress v/s strain graph with inital linear elasticity and showing non linear region(plastic region) 

    Test command : python test_anisotropic.py

    Remarks : Test case passed successfully
    
    '''
    
    strain_list_final= [np.zeros((6,1))]
    stress_list_final=[np.zeros((6,1))]
    
    ###################Ansisotopic Elasticity matrix##################
    E1=95238
    E2=95238
    E3=103700
    g=39000
    v=0.3
    
    C = np.zeros((6,6))
    
    C[0,0]=C[1,1]=E1
    C[2,2]=E3
    C[3,3]=C[4,4]=C[5,5]=g
    C[0,1]=C[2,1]=v/E2
    C[0,2]=C[1,2]=-v/E3
    C[2,0]=C[1,0]=-v/E1
    
    delta_T=0.0025
    de=0.0005                       #strain increment
    load_range=2                    #range of load
    if load_range==1:
      emax=0.015
    elif load_range==2:
      emax=0.025;

      
    
    step0=emax/de + 1
    step0=int(step0)
    tensile_load = np.linspace(0,emax,step0,endpoint=True) # progressive tensile load
    strain11=tensile_load
    steps=strain11.size-1  
    
    step=1
    
    ############Intiaizing variables####################
   
    plastic_strain=np.zeros([6,1])
    eff_plastic_strain=0
    back_stress1=np.zeros([6,1])
    back_stress2=np.zeros([6,1])
    back_stress3=np.zeros([6,1])
    drag_stress=0
    
    
    ############creating empty array###################
    stress11   = np.empty(steps+1);
    stress22   = np.empty(steps+1);
    stress33   = np.empty(steps+1);
    stress12   = np.empty(steps+1);
    stress13   = np.empty(steps+1);
    stress23   = np.empty(steps+1);
    
    strain22   = np.empty(steps+1);
    strain33   = np.empty(steps+1);
    plastic11    = np.empty(steps+1);
    
    #calling material routine for all the steps of strain increment############
    for n in range(steps):
        print(n)
        epsilon1=np.array([1,-0.5,-0.5,0,0,0])
        
        epsilon=np.multiply(epsilon1,strain11[n+1]) 
        strain22[n+1]=epsilon[1]
        strain33[n+1]=epsilon[2]
        strain_list_final.append(epsilon)
        
        C_ep,stress_new,back_stress1,back_stress2,back_stress3,drag_stress,plastic_strain,eff_plastic_strain=material_routine(C,epsilon,plastic_strain,back_stress1,back_stress2,back_stress3,drag_stress,eff_plastic_strain,step,delta_T)
        
        stress_list_final.append(stress_new)
        stress11[n+1]=stress_new[0]
        stress22[n+1]=stress_new[1]
        stress33[n+1]=stress_new[2]
        stress12[n+1]=stress_new[3]
        stress13[n+1]=stress_new[4]
        stress23[n+1]=stress_new[5]
        
        plastic11[n+1]=plastic_strain[0]
        
        step +=1
        
        
    ##plotting strain11 v/s stress11############   
    fig1, ax = plt.subplots()
    fig1.set_size_inches(16,9)
    ax.plot(strain11[:],stress11[:], label = r'$\sigma_{11}$')
    ax.set_xlabel(r'$\epsilon_{11}$', fontsize = 16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    ax.set_ylabel(r'$\sigma$ in MPa',fontsize = 16)
    ax.legend(loc=5,prop={'size': 12})
    plt.grid(color = 'black', linestyle = '--', linewidth = 0.5)
    plt.show()
    
    
    ##plotting strain increment along with increasing in steps##############
    fig3 = plt.figure()
    fig3.set_size_inches(16,9)
    x=np.linspace(0,steps,steps+1);
    plt.xlabel(r'step number', fontsize = 16)
    plt.ylabel(r'$\epsilon_{11}$ ', fontsize = 16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.plot(x,strain11,marker='o');
    plt.grid(color = 'grey', linestyle = '--', linewidth = 0.5)
    plt.show()

if __name__ == '__main__':
    test_hill()