# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# test_displacement_patch.py - Python test file testing displacement at mid nodes of two elements
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np
from Functions import *
from Element_routine_linear import *


def displacement_patch_test():
    '''
    PATCH TESTING
    Aim : A two element is subjected to  prescribed displacement in the x direction on right edges.

    Expected result : The displacements at all the middle nodes should be half of the diplacement applied on right side.

    Test Command : test_displacement_patch.py::displacement_patch_test

    Remarks : Test case passed successfully
    '''
    
    #######Elasticity matrix##########
    
    E=210e3;
    NU=0.3;
    D =(E/((1+NU)*(1-2*NU)))* np.array([[1-NU, NU ,NU ,0 ,0,0],[NU,1-NU, NU, 0, 0, 0 ],\
                                        [NU,NU ,1-NU ,0, 0 ,0 ],[0 ,0 ,0, (1-2*NU)/2, 0 ,0 ], \
                                        [0, 0, 0, 0, (1- 2*NU)/2 ,0] ,[ 0, 0, 0 ,0 ,0 ,(1-2*NU)/2]])
        
    
        
    nelx=2 #number of elements in X-direction
    nely=1 #number of elements in Y-direction
    nelz=1 #number of elements in Z-direction
    L=2    #Length 
    W=1;   #width 
    H=2;   #Height
    
    
    connectivity,coord=concoord(L,W,H,nelx,nely,nelz)   # element and node list 
    
    NOE=nelx*nely*nelz                    #Number of elements
    NON=(nelx+1)*(nely+1)*(nelz+1)        #Number of Nodes
    
    t_tot=0.25                            #total time for Simulation
    delta_T=0.025                       
    time_inc=int(t_tot/delta_T)           #time increment in seconds
    F=10
    
    
    u_k_list =[np.zeros([3*NON,1])]    
    
    strain_list_final= [np.zeros((8,6))]
    
    stress_list_final=[np.zeros((8,6))]
    
    F_ext=np.zeros([3*NON,1])
    
    for i in range(time_inc):
        #print(i)
        u_k=u_k_list[-1]
        u_k[24]=u_k[27]=u_k[30]=u_k[33]=2
    
        K_t=np.zeros([3*NON,3*NON])
        F_int=np.zeros([3*NON,1])
        
        for en in range(NOE):
            #print(en)
            
            con=np.array(connectivity[en],dtype=int)
            
            u_e=np.zeros([24,1])
            l=0
            for e in con:
                u_e[3*l:3*(l+1)]=np.array(u_k[3*int(e-1):3*int(e)]).reshape((3,1))
                l+=1
            #print(u_e)
            
            stress_list,strain_list,K_ele,F_int_ele=element_routine(D,en,connectivity,coord,u_e)
            
            
            for i in range(len(con)):
                for j in range(len(con)):
                    K_t[3*(int(con[i])-1):3*int((con[i])),3*(int(con[j])-1):3*(int(con[j]))] += K_ele[(3*i):(3*(i+1)),(3*j):(3*(j+1))]
            
            #print(K_t)
            
            for k in range(len(con)):
                F_int[3*(con[k]-1):3*con[k]]+=F_int_ele[3*k:3*(k+1)]
            
        R_k=F_int-F_ext
            
        R_k_red=np.delete(R_k,(0,1,2,3,4,5,6,7,8,9,10,11,13,14,16,17,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35),0)
        u_k_red=np.delete(u_k,(0,1,2,3,4,5,6,7,8,9,10,11,13,14,16,17,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35),0)
        K_t_red=np.delete(np.delete(K_t,(0,1,2,3,4,5,6,7,8,9,10,11,13,14,16,17,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35),0),(0,1,2,3,4,5,6,7,8,9,10,11,13,14,16,17,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35),1)
        K_t_inverse_red=np.linalg.inv(K_t_red)
        delta_uk=-K_t_inverse_red@R_k_red
    
if __name__ == '__main__':
    displacement_patch_test()