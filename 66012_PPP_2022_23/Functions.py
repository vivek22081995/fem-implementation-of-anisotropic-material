# AUTHOR:VIVEK SHIVASAGARA VIJAY
# MATRICULATION NUMBER : 66012
# Personal Programming Project

#-----------------------------------------------------------------------------------------------------------------------#
# Functions.py - Python file of different functions used in material routine , element routine and main routine
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np
import math


def hill(eq_stress,N_mat):
    '''
    This function computes the Hill criterion value for a given equivalent stress and N_matrix given in equation 4

    INPUTS :
        eq_stress             : ndarray 
                                Equivalent stress
        N_mat                 : ndarray
                                Anisotropic constants written in Matrix form 
    
    
    RETURNS :
        hill                  : scalar value
                                Hill criteria value based on equation 3

    TEST CASE :
        Test command - pytest Unit_tests.py::test_hill_criteria   
    '''
    term=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    hill=np.sqrt(term)
    return hill


def df_stress(N_mat,eq_stress):
    '''
    This function computes the first derivative Hill criterion value for a given equivalent stress and N_matrix given in equation 4

    INPUTS :
        eq_stress             : ndarray 
                                Equivalent stress
        N_mat                 : ndarray
                                Anisotropic constants written in Matrix form 
    
    
    RETURNS :
        df_stress              : ndarray
                                array of size 6*1 

    TEST CASE :
        Test command - pytest Unit_tests.py::test_first_derivative_yield_function
   
    '''
        
    term1=np.matmul(N_mat,eq_stress)
    term2=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    term3=np.sqrt(term2)
    df_stress=term1/term3
    return df_stress


def dfd_stress(N_mat,eq_stress):
    
    '''
    This function computes the second derivative Hill criterion value for a given equivalent stress and N_matrix given in equation 4

    INPUTS :
        eq_stress             : ndarray 
                                Equivalent stress
        N_mat                 : ndarray
                                Anisotropic constants written in Matrix form 
    
    
    RETURNS :
        df_stress              : ndarray
                                array of size 6*6

    TEST CASE :
        Test command - pytest Unit_tests.py::test_second_derivative_yield_function
   
    '''
    term1=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    term2=term1*N_mat
    term3=np.matmul(N_mat,eq_stress)
    term4=np.dot(term3,term3.T)
    numerator=term2-term4
    term5=np.matmul(eq_stress.T,np.matmul(N_mat,eq_stress))
    denominator=term5**(3/2)
    dfd_stress=numerator/denominator
    return dfd_stress


############Function to find an inverse of a matrix##############
def inverse_matrix(A):
     
    '''
    This function computes the inverse of a matrix 

    INPUTS :
        A                     : ndarray
                                Matrix of any size
       
    
    
    RETURNS :
        A_inversre            : ndarray
                                inverse matrix of A matrix
    '''
    N=len(A)
    I=np.identity(N)
    AM=A.copy()
    IM=I.copy()
    Index=list(range(len(A)))
    #print(Index[0:0]+Index[1:])
    for i in range(0,len(A)):
        fdscal=1.0/ AM[i][i]
        #print(fdscal)
        #print(AM)
        for j in range(len(A)):
            AM[i][j] *= fdscal
            IM[i][j] *= fdscal
        for k in Index[0:i]+Index[i+1:]:
            crscal=AM[k][i]
            for l in range(len(A)):
                AM[k][l] = AM[k][l] - crscal * AM[i][l]
                IM[k][l] = IM[k][l] - crscal * IM[i][l]     
    return IM

def concoord(L,W,H,nelx,nely,nelz):
    
    '''
    This function computes the element list and node coordinates of a given number of elements in x,y, and z directions and the Length, Width, and height of an element

    INPUTS :
        L                     : Scalar value 
                                Length
        W                     : Scalar value 
                                width
        H                     : Scalar value 
                                Height
        nelx                  : Scalar value 
                                number of elements in the X-direction
        nely                  : Scalar value 
                                number of elements in the Y-direction
        nelz                  : Scalar value 
                                number of elements in the Z-direction
    
    
    RETURNS :
        connectivity          : ndarray
                                array containing all the nodes of an element
        coord                 : ndarray
                                array containing all the coordinates of a nodes in an element
    TEST CASE :
        Test command - pytest Unit_tests.py::test_node_element_list
   
    '''
    
    NOE=nelx*nely*nelz
    k=0
    connectivity=np.zeros((NOE,8))
    for i in range(1,NOE+1):
        elx=np.fix((i-1)/(nely*nelz))+1
        ely=np.fix(((i-(elx-1)*nely*nelz)-1)/nelz)+1
        elz=i%nelz
        if elz==0:
            elz=nelz
        n1=((nely+1)*(nelz+1)*(elx-1))+((ely-1)*(nelz+1))+elz
        n2=n1+((nely+1)*(nelz+1))
        n3=n2+(nelz+1)
        n4=n1+(nelz+1)
        n5=n1+1
        n6=n2+1
        n7=n3+1
        n8=n4+1
        connectivity[k,:]=[n1,n2,n3,n4,n5,n6,n7,n8]
        k+=1
        
#######################################################
    NON=(nelx+1)*(nely+1)*(nelz+1)
    coord=np.zeros((NON,3))
    dx=L/nelx
    dy=W/nely
    dz=H/nelz
    m=0
    for ii in range(1,NON+1):
        nx=np.fix((ii-1)/((nely+1)*(nelz+1)))+1
        ny=np.fix((((ii-(nx-1)*(nely+1)*(nelz+1))-1)/(nelz+1)))+1
        nz=(ii)%(nelz+1)
        if nz==0:
            nz=nelz+1
        coord[m,:]=[(nx-1)*dx,(ny-1)*dy,(nz-1)*dz];
        m+=1
        
    return connectivity,coord


def deri_N_mat(s,t,u):
    '''
    This function gives the array of first derivative of shape functions for a given local coordinates

    INPUTS :
        s,t,u                : scalar value
                               Local coordinates 
    
    
    RETURNS :
        dN_mat               : ndarray
                               array of size 8*3

    TEST CASE :
        Test command - pytest Test_for_FEM.py::test_derivative_shape_function_sum
   
    '''
    dN1s = -((t - 1)*(u - 1))/8      ##### differentiating wrt s ######
    dN2s = ((t - 1)*(u - 1))/8
    dN3s = -((t + 1)*(u - 1))/8
    dN4s =((t + 1)*(u - 1))/8
    dN5s = ((t - 1)*(u + 1))/8
    dN6s = -((t - 1)*(u + 1))/8
    dN7s = ((t + 1)*(u + 1))/8
    dN8s = -((t + 1)*(u + 1))/8
    
    dN1t = -(s/8 - 1/8)*(u - 1)      ##### differentiating wrt t ######
    dN2t = (s/8 + 1/8)*(u - 1)
    dN3t = -(s/8 + 1/8)*(u - 1)
    dN4t = (s/8 - 1/8)*(u - 1)
    dN5t = (s/8 - 1/8)*(u + 1)
    dN6t = -(s/8 + 1/8)*(u + 1)
    dN7t = (s/8 + 1/8)*(u + 1)
    dN8t = -(s/8 - 1/8)*(u + 1)
    
    dN1u = -(s/8 - 1/8)*(t - 1)      ###### differentiating wrt u ##########
    dN2u = (s/8 + 1/8)*(t - 1)
    dN3u = -(s/8 + 1/8)*(t + 1)
    dN4u = (s/8 - 1/8)*(t + 1)
    dN5u = (s/8 - 1/8)*(t - 1)
    dN6u = -(s/8 + 1/8)*(t - 1)
    dN7u = (s/8 + 1/8)*(t + 1)
    dN8u = -(s/8 - 1/8)*(t + 1)
    
    dN_mat=np.array([[dN1s,dN2s,dN3s,dN4s,dN5s,dN6s,dN7s,dN8s],\
                    [dN1t,dN2t,dN3t,dN4t,dN5t,dN6t,dN7t,dN8t],\
                    [dN1u,dN2u,dN3u,dN4u,dN5u,dN6u,dN7u,dN8u]])
    return dN_mat

def shape_functions(s,t,u):
    '''
    This function gives the array  of shape functions for a given local coordinates

    INPUTS :
        s,t,u                : scalar value
                               Local coordinates 
    
    
    RETURNS :
        N1,N2,N3,N4
        N5,N6,N7,N8          : array
                               value of shape functions for a given local coordinate

    TEST CASE :
        Test command - pytest Test_for_FEM.py::test_shape_function_sum
   
    '''
    
    N1 = 1/8*(1-s)*(1-t)*(1-u) 
    N2 = 1/8*(1+s)*(1-t)*(1-u)
    N3 = 1/8*(1+s)*(1+t)*(1-u)
    N4 = 1/8*(1-s)*(1+t)*(1-u)
    N5 = 1/8*(1-s)*(1-t)*(1+u)
    N6 = 1/8*(1+s)*(1-t)*(1+u)
    N7 = 1/8*(1+s)*(1+t)*(1+u) 
    N8 = 1/8*(1-s)*(1+t)*(1+u)
    
    
    return N1,N2,N3,N4,N5,N6,N7,N8

def shape_functions_quadratic(s,t,u):
    '''
    This function gives the array  of shape functions for a given local coordinates

    INPUTS :
        s,t,u                : scalar value
                               Local coordinates 
    
    
    RETURNS :
        N1,N2,N3,N4,N5,N6,N7,N8,
        N9,N10,N11,N12,N13,N14,
        N15,N16,N17,N18,N19,N20  : array
                                 value of shape functions for a given local coordinate

    TEST CASE :
        Test command - pytest Test_for_FEM.py::test_quad_shape_function_sum
   
    '''
    
    N1=(1/8)*(1-s)*(1-t)*(1-u)*(-s-t-u-2)
    N2=(1/4)*(1-s)*(1-t)*(1-u**2)
    N3=(1/8)*(1-s)*(1-t)*(1+u)*(-s-t+u-2)
    N4=(1/4)*(1-s**2)*(1-t)*(1+u)
    N5=(1/8)*(1+s)*(1-t)*(1+u)*(s-t+u-2)
    N6=(1/4)*(1+s)*(1-t)*(1-u**2)
    N7=(1/8)*(1+s)*(1-t)*(1-u)*(s-t-u-2)
    N8=(1/4)*(1-s**2)*(1-t)*(1-u)
    N9=(1/4)*(1-s)*(1-t**2)*(1-u)
    N10=(1/4)*(1-s)*(1-t**2)*(1+u)
    N11=(1/4)*(1+s)*(1-t**2)*(1+u)
    N12=(1/4)*(1+s)*(1-t**2)*(1-u)
    N13=(1/8)*(1-s)*(1+t)*(1-u)*(-s+t-u-2)
    N14=(1/4)*(1-s)*(1+t)*(1-u**2)
    N15=(1/8)*(1-s)*(1+t)*(1+u)*(-s+t+u-2)
    N16=(1/4)*(1-s**2)*(1+t)*(1+u)
    N17=(1/8)*(1+s)*(1+t)*(1+u)*(s+t+u-2)
    N18=(1/4)*(1+s)*(1+t)*(1-u**2)
    N19=(1/8)*(1+s)*(1+t)*(1-u)*(s+t-u-2)
    N20=(1/4)*(1-s**2)*(1+t)*(1-u)
        
    return N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20
       
def der_quad_shape_functions(s,t,u):
    '''
    This function gives the array of first derivative of shape functions for a given local coordinates

    INPUTS :
        s,t,u                : scalar value
                               Local coordinates 
    
    
    RETURNS :
        dN_mat               : ndarray
                               array of size 24*3

    TEST CASE :
        Test command - pytest Test_for_FEM.py::test_quad_der_shape_function
   
    '''
    dN1s=(1/8)*(1-t)*(1-u)*(2*s+t+u+1)       ##############diff wrt s##############
    dN2s=(-1/4)*(1-t)*(1-u**2)
    dN3s=(1/8)*(1-t)*(1+u)*(2*s+t-u+1)
    dN4s=(-1/2)*s*(1-t)*(1+u)
    dN5s=(1/8)*(1-t)*(1+u)*(2*s-t+u-1)
    dN6s=(1/4)*(1-t)*(1-u**2)
    dN7s=(1/8)*(1-t)*(1-u)*(2*s-t-u-1)
    dN8s=(-1/2)*s*(1-t)*(1-u)
    dN9s=(-1/4)*(1-t**2)*(1-u)
    dN10s=(-1/4)*(1-t**2)*(1+u)
    dN11s=(1/4)*(1-t**2)*(1+u)
    dN12s=(1/4)*(1-t**2)*(1-u)
    dN13s=(1/8)*(1+t)*(1-u)*(2*s-t+u+1)
    dN14s=(-1/4)*(1+t)*(1-u**2)
    dN15s=(1/8)*(1+t)*(1+u)*(2*s-t-u+1)
    dN16s=(-1/2)*(s)*(1+t)*(1+u)
    dN17s=(1/8)*(1+t)*(1+u)*(2*s+t+u-1)
    dN18s=(1/4)*(1+t)*(1-u**2)
    dN19s=(1/8)*(1+t)*(1-u)*(2*s+t-u-1)
    dN20s=(-1/2)*(s)*(1+t)*(1-u)
    
    dN1t=(1/8)*(1-s)*(1-u)*(s+2*t+u+1)       ##############diff wrt s##############
    dN2t=(-1/4)*(1-s)*(1-u**2)
    dN3t=(1/8)*(1-s)*(1+u)*(s+2*t-u+1)
    dN4t=(-1/4)*(1-s**2)*(1+u)
    dN5t=(1/8)*(1+s)*(1+u)*(-s+2*t-u+1) 
    dN6t=(-1/4)*(1+s)*(1-u**2)
    dN7t=(1/8)*(1+s)*(1-u)*(-s+2*t+u+1)
    dN8t=(-1/4)*(1-s**2)*(1-u)
    dN9t=(-1/2)*(1-s)*(t)*(1-u)
    dN10t=(-1/2)*(1-s)*(t)*(1+u)
    dN11t=(-1/2)*(1+s)*(t)*(1+u)
    dN12t=(-1/2)*(1+s)*(t)*(1-u)
    dN13t=(1/8)*(1-s)*(1-u)*(-s+2*t-u-1)
    dN14t=(1/4)*(1-s)*(1-u**2)
    dN15t=(1/8)*(1-s)*(1+u)*(-s+2*t+u-1)
    dN16t=(1/4)*(1-s**2)*(1+u)
    dN17t=(1/8)*(1+s)*(1+u)*(s+2*t+u-1)
    dN18t=(1/4)*(1+s)*(1-u**2)
    dN19t=(1/8)*(1+s)*(1-u)*(s+2*t-u-1)
    dN20t=(1/4)*(1-s**2)*(1-u)
    
    dN1u=(1/8)*(1-s)*(1-t)*(s+t+2*u+1)       ##############diff wrt s##############
    dN2u=(-1/2)*(1-s)*(1-t)*(u)
    dN3u=(1/8)*(1-s)*(1-t)*(-s-t+2*u-1)
    dN4u=(1/4)*(1-s**2)*(1-t)
    dN5u=(1/8)*(1+s)*(1-t)*(s-t+2*u-1)
    dN6u=(-1/2)*(1+s)*(1-t)*(u)
    dN7u=(1/8)*(1+s)*(1-t)*(-s+t+2*u+1)
    dN8u=(-1/4)*(1-s**2)*(1-t)
    dN9u=(-1/4)*(1-s)*(1-t**2)
    dN10u=(1/4)*(1-s)*(1-t**2)
    dN11u=(1/4)*(1+s)*(1-t**2)
    dN12u=(-1/4)*(1+s)*(1-t**2)
    dN13u=(1/8)*(1-s)*(1+t)*(s-t+2*u+1)
    dN14u=(-1/2)*(1-s)*(1+t)*(u)
    dN15u=(1/8)*(1-s)*(1+t)*(-s+t+2*u-1)
    dN16u=(1/4)*(1-s**2)*(1+t)
    dN17u=(1/8)*(1+s)*(1+t)*(s+t+2*u-1)
    dN18u=(-1/2)*(1+s)*(1+t)*(u)
    dN19u=(1/8)*(1+s)*(1+t)*(-s-t+2*u+1)
    dN20u=(-1/4)*(1-s**2)*(1+t)
    
    dN_mat=np.array([[dN1s,dN2s,dN3s,dN4s,dN5s,dN6s,dN7s,dN8s,dN9s,dN10s,dN11s,dN12s,dN13s,dN14s,dN15s,dN16s,dN17s,dN18s,dN19s,dN20s],\
                    [dN1t,dN2t,dN3t,dN4t,dN5t,dN6t,dN7t,dN8t,dN9t,dN10t,dN11t,dN12t,dN13t,dN14t,dN15t,dN16t,dN17t,dN18t,dN19t,dN20t],\
                    [dN1u,dN2u,dN3u,dN4u,dN5u,dN6u,dN7u,dN8u,dN9u,dN10u,dN11u,dN12u,dN13u,dN14u,dN15u,dN16u,dN17u,dN18u,dN19u,dN20u]])
    
    return dN_mat

