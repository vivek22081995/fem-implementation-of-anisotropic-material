
# AUTHOR : Vivek Shivasagara Vijay
# MATRICULATION NUMBER : 66012
# Personal Programming Project
#-----------------------------------------------------------------------------------------------------------------------#
# Element_routine_nonlinear.py - Python  file for non linear element routine used in main routine 
#-----------------------------------------------------------------------------------------------------------------------#

import numpy as np
from Functions import *
from Material_routine_Hill import*

def element_routine(D_e,en,connectivity,coord,u_e,epl,back_stress1,back_stress2,back_stress3,drag_stress,eff_plastic_strain,step,delta_T):
    '''
    This function calculates elemental stiffnesss matrix, F_int element ,stress and strain for each element using given displacements.

    INPUTS :
        D_e                  : ndarray
                              6*6 elatsiyity matrix
        en                  : element number
                              scalar value 
        connectivity        : ndarray
                              List of the node numbers of all the elements
        coord               : ndarray
                              List of coordinates of all nodes of an element
        u_e                 : ndarray(24*1)
                              List of displacements
        epl                 : ndarry
                              6*1 plastic strain
        back_stress1        : ndarry
                              6*1 back stress component1
        back_stress2        : ndarry
                              6*1 back stress component2
        back_stress3        : ndarry
                              6*1 back stress component3
        drag_stress         : scalar value
                              drag stress
        eff_plastic_strain  : scalar value
                              effective plastic strain
    RETURNS :
        stress_list         : list
                              Updated list of stress of every gauss points
        strain_list         : list
                              Updated list of stress of every gauss points
        K_ele               : 24*24 matrix
                              Elemental stiffness matrix of each element
        F_int_ele           : 24*1 array
                              Elemental F_int of each element     
        epl_new             : ndarry
                              6*1 plastic strain new
        back_stress_new1    : ndarry
                              6*1 new back stress component1
        back_stress_new2    : ndarry
                              6*1 new back stress component2
        back_stress_new3    : ndarry
                              6*1 new back stress component3
        drag_stress_new     : scalar value
                              new drag stress
        eff_plastic_strain_new  : scalar value
                              new effective plastic strain
    '''
   
    stress_list=np.zeros((8,6))
    strain_list=np.zeros((8,6))
    
    #gauss point value
    g=1/np.sqrt(3)
    
    #Intergration Points
    GP=np.array([[-g ,-g ,-g],[g ,-g, -g],[g ,g ,-g],[-g, g ,-g],[-g, -g, g],[g, -g, g],[g, g, g],[-g, g, g]])
    
    i=np.int(connectivity[en,0])
    j=np.int(connectivity[en,1])
    k=np.int(connectivity[en,2])
    l=np.int(connectivity[en,3])
    m=np.int(connectivity[en,4])
    n=np.int(connectivity[en,5])
    o=np.int(connectivity[en,6])
    p=np.int(connectivity[en,7])
    
    
    x1=coord[i-1,0];y1=coord[i-1,1];z1=coord[i-1,2];
    x2=coord[j-1,0];y2=coord[j-1,1];z2=coord[j-1,2];
    x3=coord[k-1,0];y3=coord[k-1,1];z3=coord[k-1,2];
    x4=coord[l-1,0];y4=coord[l-1,1];z4=coord[l-1,2];
    x5=coord[m-1,0];y5=coord[m-1,1];z5=coord[m-1,2];
    x6=coord[n-1,0];y6=coord[n-1,1];z6=coord[n-1,2];
    x7=coord[o-1,0];y7=coord[o-1,1];z7=coord[o-1,2];
    x8=coord[p-1,0];y8=coord[p-1,1];z8=coord[p-1,2];
    
    new_coord=np.array([[x1,y1,z1],[x2,y2,z2],[x3,y3,z3],[x4,y4,z4],[x5,y5,z5],[x6,y6,z6],[x7,y7,z7],[x8,y8,z8]])
    
    # =============================================================================
    #  interpolation of shape functions
    #  x = N1*x1 + N2*x2 + N3*x3 + N4*x4 + N5*x5 + N6*x6 + N7*x7 + N8*x8;
    #  y = N1*y1 + N2*y2 + N3*y3 + N4*y4 + N5*y5 + N6*y6 + N7*y7 + N8*y8;
    #  z = N1*z1 + N2*z2 + N3*z3 + N4*z4 + N5*z5 + N6*z6 + N7*z7 + N8*z8;
    # =============================================================================
    
    K_ele=np.zeros([24,24])
    
    F_int_ele=np.zeros([24,1])
    
    #iterating over all 8 gauss points
    for iter in range(8):
        s=GP[iter,0]
        t=GP[iter,1]
        u=GP[iter,2]
        
        #calculating derivative of shape functions wrt s,t and u(local cordinates)
        dN_mat=dd_N_mat(s, t, u)
        
        #Jacobian matrix
        Jacobian_matrix=np.matmul(dN_mat,new_coord)
        
        #determinant of Jacobian matrix
        detJ=np.linalg.det(Jacobian_matrix)
        
        #inverse of jacobian matrix
        Jacobian_inverse=np.linalg.inv(Jacobian_matrix)
        
        #derivative of shape functions wrt x,y and z(global coordinates)
        Nxyz=np.matmul(Jacobian_inverse,dN_mat)
        
        N1x = Nxyz[0,0]; N2x = Nxyz[0,1]; N3x = Nxyz[0,2]; N4x = Nxyz[0,3]
        N5x = Nxyz[0,4]; N6x = Nxyz[0,5]; N7x = Nxyz[0,6]; N8x = Nxyz[0,7]
        N1y = Nxyz[1,0]; N2y = Nxyz[1,1]; N3y = Nxyz[1,2]; N4y = Nxyz[1,3]
        N5y = Nxyz[1,4]; N6y = Nxyz[1,5]; N7y = Nxyz[1,6]; N8y = Nxyz[1,7]
        N1z = Nxyz[2,0]; N2z = Nxyz[2,1]; N3z = Nxyz[2,2]; N4z = Nxyz[2,3]
        N5z = Nxyz[2,4]; N6z = Nxyz[2,5]; N7z = Nxyz[2,6]; N8z = Nxyz[2,7]
        
        #B matrix to calculate strain and stiffness matrix
        B = np.array([[N1x,0,0,N2x,0,0,N3x,0,0,N4x,0,0,N5x,0,0,N6x,0,0,N7x,0,0,N8x,0,0], \
                               [0,N1y,0,0,N2y,0,0,N3y,0,0,N4y,0,0,N5y,0,0,N6y,0,0,N7y,0,0,N8y,0], \
                               [0,0,N1z,0,0,N2z,0,0,N3z,0,0,N4z,0,0,N5z,0,0,N6z,0,0,N7z,0,0,N8z], \
                               [N1y,N1x,0,N2y,N2x,0,N3y,N3x,0,N4y,N4x,0,N5y,N5x,0,N6y,N6x,0,N7y,N7x,0,N8y,N8x,0], \
                               [0,N1z,N1y,0,N2z,N2y,0,N3z,N3y,0,N4z,N4y,0,N5z,N5y,0,N6z,N6y,0,N7z,N7y,0,N8z,N8y], \
                               [N1z,0,N1x,N2z,0,N2x,N3z,0,N3x,N4z,0,N4x,N5z,0,N5x,N6z,0,N6x,N7z,0,N7x, N8z,0, N8x]])
        
        
        
        strain=np.matmul(B,u_e) # to calculate strain
        strain_list[iter,:]=strain.reshape(1,6)
        
       
        D_ep,stress_new,back_stress_new1,back_stress_new2,back_stress_new3,drag_stress_new,epl_new,eff_plastic_strain_new=material_routine(D_e,strain,epl,back_stress1,back_stress2,back_stress3,drag_stress,eff_plastic_strain,step,delta_T)
        
        
        stress_list[iter,:]=stress_new.reshape(1,6)
          
        BD=(np.matmul(B.T,np.matmul(D_ep,B)))*detJ
        K_ele=K_ele+BD
        
        F_int_ele=F_int_ele+np.matmul(B.T,stress_new)*detJ
    
    return stress_list,strain_list,K_ele,F_int_ele,epl_new,back_stress_new1,back_stress_new2,back_stress_new3,drag_stress_new,epl_new,eff_plastic_strain_new

##############################################################################################################################
''' All the functions listed below are same as the functions used in the main program  
'''
###############################################################################################################################

def dd_N_mat(s,t,u):
    dN1s = -((t - 1)*(u - 1))/8   ### differentiating wrt s ######
    dN2s = ((t - 1)*(u - 1))/8
    dN3s = -((t + 1)*(u - 1))/8
    dN4s =((t + 1)*(u - 1))/8
    dN5s = ((t - 1)*(u + 1))/8
    dN6s = -((t - 1)*(u + 1))/8
    dN7s = ((t + 1)*(u + 1))/8
    dN8s = -((t + 1)*(u + 1))/8
    
    dN1t = -(s/8 - 1/8)*(u - 1)      ##### differentiating wrt t ######
    dN2t = (s/8 + 1/8)*(u - 1)
    dN3t = -(s/8 + 1/8)*(u - 1)
    dN4t = (s/8 - 1/8)*(u - 1)
    dN5t = (s/8 - 1/8)*(u + 1)
    dN6t = -(s/8 + 1/8)*(u + 1)
    dN7t = (s/8 + 1/8)*(u + 1)
    dN8t = -(s/8 - 1/8)*(u + 1)
    
    dN1u = -(s/8 - 1/8)*(t - 1)      ###### differentiating wrt u ##########
    dN2u = (s/8 + 1/8)*(t - 1)
    dN3u = -(s/8 + 1/8)*(t + 1)
    dN4u = (s/8 - 1/8)*(t + 1)
    dN5u = (s/8 - 1/8)*(t - 1)
    dN6u = -(s/8 + 1/8)*(t - 1)
    dN7u = (s/8 + 1/8)*(t + 1)
    dN8u = -(s/8 - 1/8)*(t + 1)
    
    dN_mat=np.array([[dN1s,dN2s,dN3s,dN4s,dN5s,dN6s,dN7s,dN8s],\
                    [dN1t,dN2t,dN3t,dN4t,dN5t,dN6t,dN7t,dN8t],\
                    [dN1u,dN2u,dN3u,dN4u,dN5u,dN6u,dN7u,dN8u]])
    return dN_mat

def shape_functions(s,t,u):
    N1 = 1/8*(1-s)*(1-t)*(1-u) 
    N2 = 1/8*(1+s)*(1-t)*(1-u)
    N3 = 1/8*(1+s)*(1+t)*(1-u)
    N4 = 1/8*(1-s)*(1+t)*(1-u)
    N5 = 1/8*(1-s)*(1-t)*(1+u)
    N6 = 1/8*(1+s)*(1-t)*(1+u)
    N7 = 1/8*(1+s)*(1+t)*(1+u) 
    N8 = 1/8*(1-s)*(1+t)*(1+u)
    
    N_vec=np.array([N1,N2,N3,N4,N5,N6,N7,N8])
    
    return N_vec

